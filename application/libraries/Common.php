<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Common {
	public $CI;
	function __construct() {
		$this ->CI =& get_instance();
	}
	function active_class($controller,$type = 'c'){
		if($type == 'c')
			return $this->CI->router->fetch_class() == $controller?'home':'';
		else
			return $this->CI->router->fetch_method() == $controller?'home':'';
	}
	function Auth(){
      if($this->CI->session->userdata('AdminId')!= '' && $this->CI->session->userdata('Email')!=''){
				return 0;
			}
			else{return 1;}
  	}
  	public function post_data($url,$data)
  	{
		$options = array(
		    'http' => array(
		        'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
		        'method'  => 'POST',
		        'content' => http_build_query($data)
		    )
		);
		$context  = stream_context_create($options);
		$data = file_get_contents($url, false, $context);
		$data = json_decode($data);
		if ($data === FALSE) {

		} else {
			return $data;
		}
  	}
  	public function uploadfile($url,$data)
  	{
		define('MULTIPART_BOUNDARY', '--------------------------'.microtime(true));
		$options = array(
		    'http' => array(
		        'header'  => 'Content-Type: multipart/form-data;boundary='.MULTIPART_BOUNDARY,
		        'method'  => 'POST',
		        'content' => http_build_query($data)
		    )
		);
		$context  = stream_context_create($options);
		$data = file_get_contents($url, false, $context);
		$data = json_decode($data);
		if ($data === FALSE) {

		} else {
			return $data;
		}
  	}
  	public function get_data($url)
  	{
  		$data = file_get_contents($url);
  		$data = json_decode($data);
		if ($data === FALSE) { 
		/* Handle error */ } else {
			return $data;			
		}	
  	}
	public function check_day_difference($date)
	{
		$date1 = date_create(date("Y-m-d"));
		$date2 = date_create($date);
		$diff = date_diff($date1,$date2);
		$res = $diff->format("%R%a");
		return $res;
	}
	//countroller Function
	public function processandredirect($res,$success,$error,$cname)
	{
		if($res > 0)
		{
			$this->CI->session->set_flashdata('phpsuccess', $success);
			redirect(base_url($cname));
		}
		else
		{
			$this->CI->session->set_flashdata('phperror',$error);
			redirect(base_url($cname));
		}
	}
}
?>