<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}
	public function index()
	{
		$this->load->view('home');
	}
	public function login()
	{
		$this->load->library('braintree_lib');
		$card = array(
		'number' => '5105105105105100',
		'cardholderName' => 'abc abc',
		'expirationDate' => '12/24',
		'cvv' => '123'
		);
		$data = $this->braintree_lib->transaction($card,'100.00');
		echo $data; exit;
		//echo "<pre>"; print_r($data); exit;
	}
	public function about()
	{
		$this->load->view('about');
	}
	public function member()
	{
		$data = $this->common->get_data('http://35.163.229.242/Master_trader/index.php/api/User/get_plan');
		$this->load->view('membership',$data);
	}
	public function getplan()
	{
		$pass = array('plan_id' => '4');
		$data = $this->common->post_data('http://35.163.229.242/Master_trader/index.php/api/User/get_plan_detail',$pass);
		echo $data->plan_data->amount;
		echo "<pre>"; print_r($data); exit;	
	}
	public function getnotification()
	{
		$pass = array('auth_token' => $this->session->userdata('AuthToken'));
		$data = $this->common->post_data('http://35.163.229.242/Master_trader/index.php/api/User/get_notification',$pass);
		//echo "<pre>"; print_r($data); exit;
		$return = '';
		$output = array();
		$noti = array();
		if( $data->status != 0){
         $noti = $data->notification_data;
         foreach ($noti as $value) {
         	$return .= '<li role="presentation"><a role="menuitem" tabindex="-1" href="#" style="color: #DAA520;"> '.$value->notification.'</a></li>
                <li role="presentation"><a role="menuitem" tabindex="-1" href="#" style="color:#D3D3D3;"> '.date(" F d, Y g:i A",strtotime($value->created_on)).'</a></li><li role="presentation"></li><li role="presentation" class="divider"></li>';
         }
        }
        else{
        $return .= '<li role="presentation"><a role="menuitem" tabindex="-1" href="#" style="color: #DAA520;"></a></li>
                <li role="presentation"><a role="menuitem" tabindex="-1" href="#" style="color: red;"></a></li><li role="presentation"><a role="menuitem" tabindex="-1" href="#"><b></b></a></li><li role="presentation"><a role="menuitem" tabindex="-1" href="#" style="color: black;"> </a></li><li role="presentation" class="divider"></li>';
        }
        $output = array('total'=>count($noti),'return'=>$return);
		echo json_encode($output);
        //echo $return;
	}
	public function getalert()
	{
		$data = $this->common->post_data('http://35.163.229.242/Master_trader/index.php/api/User/get_alerts',array('auth_token' => $this->session->userdata('AuthToken'),'page_nu'=>'0'));
		//echo "<pre>"; print_r($data); exit;	
		$return = '';
		$output = array();
		$alert = array();
        if( $data->status != 0){
         $alert = $data->alerts_data;
         foreach ($alert as $value) {
            $color_code=($value->type == "SELL" || $value->type == "SELL SHORT")?'red':'#008000';
         	$return .= '<a role="menuitem" tabindex="-1" href="#" style="color: #DAA520;">$'.$value->symbol.'</a><a role="menuitem" tabindex="-1" href="#" style="color: '.$color_code.';"> '.$value->type.'</a><a role="menuitem" tabindex="-1" href="#" style="color: #000;"><b> $'.$value->price.'</b></a><br><a role="menuitem" tabindex="-1" href="#" style="color: black;"> '.$value->comment.'</a><li role="presentation" ><a style="color:#D3D3D3;" role="menuitem"><b> '.date(" F d, Y g:i A",strtotime($value->created_on)).'</b></a></li><li role="presentation" class="divider"></li>';
         }
        }
        else{
         $return .= '<a role="menuitem" tabindex="-1" href="#" style="color: #DAA520;"> </a><a role="menuitem" tabindex="-1" href="#"><b> </b></a><a role="menuitem" tabindex="-1" href="#" style="color: red;"></a><a role="menuitem" tabindex="-1" href="#" style="color: black;"></a><li role="presentation" ><a role="menuitem"><b></b></a></li><li role="presentation" class="divider"></li>';
        }
        $output = array('total'=>count($alert),'return'=>$return);
        //echo "<pre>"; print_r($output); exit;	
        echo json_encode($output);
        //echo $return;
	}	
	public function Term_And_Conditions()
	{
		$this->load->view('term_and_condition');
	}

	public function Disclaimer()
	{
		$this->load->view('disclaimers');
	}
	public function martext()
	{
		$massage = $this->common->get_data('http://35.163.229.242/Master_trader/index.php/api/User/get_website_text');
 		$data = $massage->website_text;
        echo  $data->website_text;
	}
}

/* End of file Home.php */
/* Location: ./application/controllers/Home.php */