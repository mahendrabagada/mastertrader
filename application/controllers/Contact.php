<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller {

	function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
		$this->load->view('contact');
	}
	
	public function Submit(){

		
		$this->form_validation->set_rules('name','Name','required');
		$this->form_validation->set_rules('email','Email','required');
		$this->form_validation->set_rules('subject','Subject','required');
		$this->form_validation->set_rules('message','Message','required');

		if($this->form_validation->run()){


		$name = $this->input->post('name');
		$email = $this->input->post('email');
		$subject = $this->input->post('subject');
		$message = $this->input->post('message');

		$url = 'http://35.163.229.242/Master_trader/index.php/api/User/contact';
		$data = array('name' => $name, 'email' => $email,'subject' => $subject, 'message' => $message);

		// use key 'http' even if you send the request to https://...
		$options = array(
		    'http' => array(
		        'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
		        'method'  => 'POST',
		        'content' => http_build_query($data)
		    )
		);
		$context  = stream_context_create($options);
		$data = file_get_contents($url, false, $context);
		$data = json_decode($data);
		if($data->status == 1)
			$this->session->set_flashdata('phpsuccess',$data->message);
		else
			$this->session->set_flashdata('phperror',$data->message);
		$this->load->view('contact');
		}

	}

}

/* End of file Contact.php */
/* Location: ./application/controllers/Contact.php */