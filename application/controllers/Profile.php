<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller {
	function __construct()
	{
		parent::__construct();
		
	}

	public function index()
	{
		if($this->session->userdata('UserName') == '' || $this->session->userdata('UserLoginStatus') == '2')
			redirect(base_url('Home'));
		$getplan = $this->common->post_data('http://35.163.229.242/Master_trader/index.php/api/User/get_plan_detail',array('plan_id' => $this->session->userdata('UserPlanId')));
		$data['plan'] = $getplan->plan_data;
		//echo '<pre>'; print_r($data['plan']); exit;
		$this->load->view('profile_view',$data);
	}
	public function change_password()
	{
		if($this->session->userdata('UserName') == '' || $this->session->userdata('UserLoginStatus') == '2')
			redirect(base_url('Home'));
		$old = $this->input->post('Old_Password');
		$new = $this->input->post('Password');
		$this->form_validation->set_rules('Old_Password','Old Password','required');
		$this->form_validation->set_rules('Password','User Password','required');
		if ($this->form_validation->run() == TRUE){
			$where = array('auth_token' => $this->session->userdata('AuthToken'),'password' => $new);
			$res = $this->common->post_data('http://35.163.229.242/Master_trader/index.php/api/User/change_password',$where);
			if($res->status == 1){
				$this->session->set_flashdata('phpsuccess','Password Changed Successfully');
				redirect(base_url('Profile'));
			}
			else{
				$this->session->set_flashdata('phperror','Password Not Changed!!!!!!');
				redirect(base_url('Profile'));
			}
		}
		else
		{
			$this->session->set_flashdata('phperror','Some Error Occure While Fill The Form!!!!'.validation_errors());
			redirect(base_url('Login'));
		}
	}
	public function profilepicture() {
        if ($this->session->userdata('UserName') == '' || $this->session->userdata('UserLoginStatus') == '2')
            redirect(base_url('Home'));
        $data['Files'] = $_FILES;
        //echo $_FILES['filename']
        if ($_FILES['filename']['error'] == 0) {
            $allowed = array('jpg', 'jpeg', 'png');
            $ext = pathinfo($_FILES['filename']['name'], PATHINFO_EXTENSION);
            if (!in_array($ext, $allowed)) {
                $this->session->set_flashdata('phperror', 'You Trying To upload file that are not allowed.!!!');
                redirect(base_url('Profile'));
            }
            $RealTitleID = $_FILES['filename']['name'];
            $ch = curl_init("http://35.163.229.242/Master_trader/index.php/api/User/ImageUpdate");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            $where = array('auth_token' => $this->session->userdata('AuthToken'), 'profile_picture' => curl_file_create($_FILES['filename']['tmp_name'], $_FILES['filename']['type'], $RealTitleID));
            curl_setopt($ch, CURLOPT_POSTFIELDS, $where);
            $result = curl_exec($ch);
            $res = json_decode($result);
            $this->session->set_userdata('NewProfilePicture',$res->path);
            $this->common->processandredirect($res->status, 'Profile Picture Changed Successfully.!!!', 'Some Error Occure While Change The Profile Picture.' . validation_errors(), 'Profile');
        }
    }
	public function upgrade()
	{
		$data['regdata'] = array(
						'first_name' => $this->session->userdata('FirstName'),
					  'last_name' => $this->session->userdata('LastName'),
					  'email' => $this->session->userdata('Email'),
					  'username' => $this->session->userdata('UserName'),
					  'password' => '',
					  'plan_id' => $this->session->userdata('UserPlanId')
					);
		$pass = array('plan_id' => $this->session->userdata('UserPlanId'));
		$plandetail = $this->common->post_data('http://35.163.229.242/Master_trader/index.php/api/User/get_plan_detail',$pass);
		$plan = $this->common->get_data('http://35.163.229.242/Master_trader/index.php/api/User/get_plan');
		$data['plandata'] = $plan->plan_data;
		$data['amount'] = $plandetail->plan_data->amount;
		$data['plan'] = $plandetail->plan_data;
		$this->load->view('checkout',$data);
	}
	public function cancelmembership()
	{
		if($this->session->userdata('UserName') == '' || $this->session->userdata('UserLoginStatus') == '2')
			redirect(base_url('Home'));
		$pass = array('auth_token' => $this->session->userdata('AuthToken'));
		$plandetail = $this->common->post_data('http://35.163.229.242/Master_trader/index.php/api/User/set_auto_renew',$pass);
		$this->session->set_flashdata('phpsuccess','Cancel Membership Successfully.!!!');
		redirect(base_url('Home'));
	}
	public function testprofile()
	{
		$data = $this->common->post_data('http://35.163.229.242/Master_trader/index.php/api/User/get_plan_detail',array('plan_id' => '4'));
		echo '<pre>'; print_r($data); exit;
		$this->load->view('profile_view');
	}
}

/* End of file Profile.php */
/* Location: ./application/controllers/Profile.php */