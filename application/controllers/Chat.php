<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Chat extends CI_Controller {

	public function index()
	{
		$data['quserid'] = $this->session->userdata('QBUserId');
		$data['qusermail'] = $this->session->userdata('Email');
		$data['quserlogin'] = $this->session->userdata('UserName');
		$data['quserpass'] = md5($this->session->userdata('QBUserPass'));
		//echo "<pre>"; print_r($data); exit;
			
	$this->load->view('chat',$data);
	}
	public function test()
	{
		$uname = 'test@gmail.com';
		$password = 'testtest';
		if($this->session->userdata('APIToken') == ''){
			$session = $this->createSession(APPLICATION_ID, AUTH_KEY, AUTH_SECRET, $uname, $password);
			$token = $session->token;			
			$this->session->set_userdata( 'APIToken',$token );
			echo 'Success';
		}
	}
	public function retriveuser()
	{
		$api = 'users.json';
		$arr = $this->getapi($api);
		$item = $arr->items;
		$login = array();
		$full = array();
		foreach ($item as $key => $value) {
			foreach ($value as $val) {
				$full[] = array('login'=>$val->login,'email'=>$val->email,'id'=>$val->id,'owner'=>$val->owner_id,'fullname'=>$val->full_name);
				array_push($login, $val->login);
			}
		}
		echo '<pre>'; print_r($arr); print_r($full); print_r($login);
	}
	public function getuser()
	{
		
		$api = 'users/by_login.json?login=test@gmail.com';
		$arr = $this->getapi($api);
		if(count($arr) > 0){
			$data['quserid'] = $arr->user->id;
			$data['qusermail'] = $arr->user->email;
			$data['quserlogin'] = $arr->user->login;
			$data['quserpass'] = 'testtest';
		}
		$this->load->view('chat',$data);
		
	}
	public function quickbloxregister($login,$password,$email)
	{
		//$arr = array('login'=>'shreekunj','password'=>'shreekunj','email'=>'shreekunj@mail.com');
		$arr = array('login'=>$login,'password'=>$password,'email'=>$email);
		$post_body = http_build_query(array('user'=>$arr));
		$arr = $this->postapi($post_body,'users.json');
		return $arr->user;
	}
	function createSession($appId, $authKey, $authSecret, $login, $password) {
			if (!$appId || !$authKey || !$authSecret || !$login || !$password) {
	    		return false;
	  		}
	  // Generate signature
	  $nonce = rand();
	  $timestamp = time();
	  $signature_string = "application_id=" . $appId . "&auth_key=" . $authKey . "&nonce=" . $nonce . "&timestamp=" . $timestamp . "&user[login]=" . $login . "&user[password]=" . $password;
	 
	  $signature = hash_hmac('sha1', $signature_string , $authSecret);
	  // Build post body
	  $post_body = http_build_query( array(
	    'application_id' => $appId,
	    'auth_key' => $authKey,
	    'timestamp' => $timestamp,
	    'nonce' => $nonce,
	    'signature' => $signature,
	    'user[login]' => $login,
	    'user[password]' => $password
	  ));
	  // Configure cURL
	  $curl = curl_init();
	  curl_setopt($curl, CURLOPT_URL, QB_API_ENDPOINT . '/' . QB_PATH_SESSION);
	  curl_setopt($curl, CURLOPT_POST, true); // Use POST
	  curl_setopt($curl, CURLOPT_POSTFIELDS, $post_body); // Setup post body
	  curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); // Receive server response
	 
	  // Execute request and read response
	  $response = curl_exec($curl);
	  $responseJSON = json_decode($response)->session;
	  // Check errors
	  if ($responseJSON) {
	    return $responseJSON;
	  } else {
	    $error = curl_error($curl). '(' .curl_errno($curl). ')';
	    return $error;
	  }
	  // Close connection
	  curl_close($curl);
	}
	public function postapi($post_body,$api)
	{
		$curl = curl_init();
	  	curl_setopt($curl, CURLOPT_URL, QB_API_ENDPOINT . '/' . $api);
	  	curl_setopt($curl, CURLOPT_HTTPHEADER, array("QB-Token: ".$this->session->userdata( 'APIToken')));
	  	curl_setopt($curl, CURLOPT_POST, true); // Use POST
	  	curl_setopt($curl, CURLOPT_POSTFIELDS, $post_body); // Setup post body
	  	curl_setopt($curl, CURLOPT_RETURNTRANSFER, true); // Receive server response
	 
		  // Execute request and read response
		$response = curl_exec($curl);
		$responseJSON = json_decode($response);
		  // Check errors
		if ($responseJSON) {
		    return $responseJSON;
		} else {
		    $error = curl_error($curl). '(' .curl_errno($curl). ')';
		    return $error;
		}
		  // Close connection
		curl_close($curl);
	}
	public function getapi($api)
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, QB_API_ENDPOINT . '/' . $api);
	  	curl_setopt($ch, CURLOPT_HTTPHEADER, array("QB-Token: ".$this->session->userdata( 'APIToken')));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$responce = curl_exec ($ch);
		$responseJSON = json_decode($responce);
		if ($responseJSON) {
        return $responseJSON;
		} else {
		        $error = array();//curl_error($ch). '(' .curl_errno($ch). ')';
		        return $error;
		}
		// Close connection
		curl_close($curl);
	}
}

/* End of file Chat.php */
/* Location: ./application/controllers/Chat.php */