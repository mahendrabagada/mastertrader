<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Checkout extends CI_Controller {

	public function index()
	{
		$this->load->view('checkout.php');		
	}

}

/* End of file Checkout.php */
/* Location: ./application/controllers/Checkout.php */