<?php 
$result = Braintree_Transaction::sale([
  'amount' => '100.00',
  'orderId' => 'order id',
  'merchantAccountId' => 'a_merchant_account_id',
  'paymentMethodNonce' => nonceFromTheClient,
  'customer' => [
    'firstName' => 'Drew',
    'lastName' => 'Smith',
    'company' => 'Braintree',
    'phone' => '312-555-1234',
    'fax' => '312-555-1235',
    'website' => 'http://www.example.com',
    'email' => 'drew@example.com'
  ],
  'billing' => [
    'firstName' => 'Paul',
    'lastName' => 'Smith',
    'company' => 'Braintree',
    'streetAddress' => '1 E Main St',
    'extendedAddress' => 'Suite 403',
    'locality' => 'Chicago',
    'region' => 'IL',
    'postalCode' => '60622',
    'countryCodeAlpha2' => 'US'
  ],
  'shipping' => [
    'firstName' => 'Jen',
    'lastName' => 'Smith',
    'company' => 'Braintree',
    'streetAddress' => '1 E 1st St',
    'extendedAddress' => 'Suite 403',
    'locality' => 'Bartlett',
    'region' => 'IL',
    'postalCode' => '60103',
    'countryCodeAlpha2' => 'US'
  ],
  'options' => [
    'submitForSettlement' => true
  ]
]);
//Transaction is succeess or false
$result->success
# true

$result->transaction->status
# e.g. 'submitted_for_settlement'

$result->transaction->type
# e.g. 'credit'

//custom field value
$result = Braintree_Transaction::sale([
    'amount' => '100.00',
    'paymentMethodNonce' => nonceFromTheClient,
    'customFields' => [
        'custom_field_one' => 'custom value',
        'custom_field_two' => 'another custom value'
    ]
]);

$result->transaction->customFields['custom_field_one']
# 'custom value'

$result->transaction->customFields['custom_field_two']
# 'another custom value'
?>