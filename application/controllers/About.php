<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class About extends CI_Controller {

	public function abouts()
	{
		$this->load->view('about');
	}

}

