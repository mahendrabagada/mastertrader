<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
	function __construct()
	{
		parent::__construct();
	}
	public function index($id = '')
	{

		if($this->session->userdata('UserName') != '' && $this->session->userdata('UserLoginStatus') == '1')
			redirect(base_url());
		$id = base64_decode($id);
		$plan = $this->common->get_data('http://35.163.229.242/Master_trader/index.php/api/User/get_plan');
		$data['plan'] = $plan->plan_data;
		$data['id'] = $id;
		$this->load->view('getstarted',$data);
	}
	public function do_login()
	{
		$this->form_validation->set_message('required', '<i style="color:red;  margin:-2px;">Please Enter Valid Username or Password !</i>');
		$this->form_validation->set_rules('UserName', 'UserName', 'required');
		$this->form_validation->set_rules('Password', 'Password', 'required');		
		if ($this->form_validation->run() == TRUE)
		{
			$where = array('username' => $this->input->post('UserName') , 'password'=> $this->input->post('Password'));
			$reffer = $this->session->userdata('last_page') ? $this->session->userdata('last_page') : base_url('Home');
			$data = $this->common->post_data('http://35.163.229.242/Master_trader/index.php/api/User/login',$where);
			if($data->status == 0){
				$this->session->set_flashdata('phperror','Could Not Login '.$data->message.'!!!');
				redirect(base_url('Login'));
			} else if($data->status == 2){
				$session_set = array(
					'UserName' => $data->user_data->username,
					'Email' => $data->user_data->email,
					'FirstName' => $data->user_data->first_name,
					'LastName' => $data->user_data->last_name,
					'AuthToken' => $data->user_data->auth_token,
					'SubExpDate' => date('Y-m-d h:i:s'),
					'UserPlanId' => '4',
					'UserLoginStatus' => '2'
				);
				$this->session->set_userdata($session_set);
				redirect(base_url('Profile/upgrade'));
			}else{
				$session_set = array(
					'UserName' => $data->user->username,
					'Email' => $data->user->email,
					'FirstName' => $data->user->first_name,
					'LastName' => $data->user->last_name,
					'AuthToken' => $data->user->auth_token,
					'SubExpDate' => $data->user->expired_on,
					'ProfilePicture' => $data->user->profile_picture,
					'ThumbImage' => $data->user->thumb_image,
					'QBUserId' => $data->user->qb_user_id,
					'CreatedOn' => $data->user->created_on,
					'PublicDiglogId' => $data->user->public_diglog_id,
					'Role' => $data->user->role,
					'UserPlanId' => $data->user->plan_id,
					'QBUserPass' => $this->input->post('Password'),
					'UserLoginStatus' => '1'
				);
				$this->session->set_userdata($session_set);
				$this->session->set_flashdata('phpsuccess','Login Successfully!!!');
				redirect(base_url('Chat'));
			}
		}
		else
		{
			$this->session->set_flashdata('phperror','Please Enter Valid Username or Password !');
			redirect(base_url('Login'));
		}
	}
	public function signup()
	{
		$id = $this->input->post('PlanId');
		$pass = array('plan_id' => $id);
		$plandetail = $this->common->post_data('http://35.163.229.242/Master_trader/index.php/api/User/get_plan_detail',$pass);
		$data['amount'] = $plandetail->plan_data->amount;
		$data['plan'] = $plandetail->plan_data;
		$data['regdata'] = array('first_name' => $this->input->post('FirstName'),
					  'last_name' => $this->input->post('LastName'),
					  'email' => $this->input->post('Email'),
					  'username' => $this->input->post('UserName'),
					  'password' => $this->input->post('Password'),
					  'plan_id' => $id
					);
		$this->form_validation->set_rules('FirstName', 'First Name', 'required');
		$this->form_validation->set_rules('LastName', 'Last Name', 'required');
		$this->form_validation->set_rules('Email', 'Email', 'required');
		$this->form_validation->set_rules('UserName', 'UserName', 'required');
		$this->form_validation->set_rules('Password', 'Password', 'required');	
		if ($this->form_validation->run() == TRUE)
		{
			$this->load->view('checkout',$data);
		} else {
			$this->session->set_flashdata('phperror','All Field Is Mandatory !');
			redirect(base_url('Login'));
		}
	}
	public function Logout()
	{
		$array_items = array('UserName' => '', 'Email' => '' ,'AuthToken' => '', 'FirstName' => '','LastName' => '','SubExpDate' => '','ProfilePicture' => '','ThumbImage' => '','QBUserId' => '','CreatedOn' => '','PublicDiglogId' => '','Role' => '','UserPlanId' => '','UserLoginStatus'=>'');
		$this->session->unset_userdata($array_items);
		$this->session->sess_destroy();
		redirect(base_url());
	}
	public function test()
	{
		$where = array('username' => 'abc' , 'password'=> '123');
		//$where = array('username' => 'testing' , 'password'=> '123456');
		$data = $this->common->post_data('http://35.163.229.242/Master_trader/index.php/api/User/login',$where);
		echo "<pre>"; print_r($data); exit;
	}
	public function maketransaction()
	{
		$d = $this->input->post();
		$data = array('first_name' => $d['first_name'],
					  'last_name' => $d['last_name'],
					  'email' => $d['email'],
					  'username' => $d['username'],
					  'password' => $d['password'],
					  'plan_id' => $d['plan_id']);
		$expirationDate = $this->input->post('ExpiryMonth').'/'.$this->input->post('ExpiryYear');
		$this->load->library('braintree_lib');
		$card = array(
		'number' => $this->input->post('CardNumber'),//'5105105105105100',
		'cardholderName' => $this->input->post('CardHolderName'),//'abc abc',
		'expirationDate' => $expirationDate,//'12/24',
		'cvv' => $this->input->post('Cvv')//'123'
		);
		$getplan = $this->common->post_data('http://35.163.229.242/Master_trader/index.php/api/User/get_plan_detail',array('plan_id' => $data['plan_id']));
		$amount = $d['amount'];
		if($d['amount'] != $getplan->plan_data->amount)
			$amount = $getplan->plan_data->amount;
		$result = $this->braintree_lib->transaction($card,$amount);
		if ($result->success) 
		{
			if($result->transaction->id)
			{
				$braintreeCode=$result->transaction->id;
				$res = array();
				if($data['password'] != ''){
				$res = $this->common->post_data('http://35.163.229.242/Master_trader/index.php/api/User/register_user',$data);
				$this->session->set_flashdata('regsuccess','You Have Successfully Registerd');

				} else {
					$plandata = array('auth_token'=>$this->session->userdata('AuthToken'),'plan_id'=>$data['plan_id']);
					$res = $this->common->post_data('http://35.163.229.242/Master_trader/index.php/api/User/upgrade_plan',$plandata);
					$this->session->set_flashdata('plan','You Have Successfully Upgrade The Plan');
					//$res = json_decode(json_encode(array('status' => 1)));
				}
				if($res->status == 1){
					$token = isset($res->auth_token->auth_token)?$res->auth_token->auth_token:$this->session->userdata('AuthToken');
					$tra = array('auth_token'=>$token,'transaction_id'=>$braintreeCode,'plan_id'=> $data['plan_id']);
					$successtra = $this->common->post_data('http://35.163.229.242/Master_trader/index.php/api/User/update_trasaction',$tra);
					if($successtra->status == 1){
						$this->session->set_flashdata('phpsuccess',$res->message);
						$this->load->view('success');
						//redirect(base_url('Home'));
					}
					
				} else {

					$msg = isset($res->message)?$res->message:'Some Error In Upgrade Plan';
					$this->session->set_flashdata('phperror',$msg);//$res->message
					redirect(base_url('Home'));
				}
			}
		} else {
			$this->session->set_flashdata('phperror','Transaction Is Canceled Some Error Occure !');
			redirect(base_url('Login'));
		}
	}
	public function Forget()
	{
		if($this->session->userdata('UserName') == '' || $this->session->userdata('UserLoginStatus') == '2')
			redirect(base_url('Home'));
		$this->form_validation->set_rules('FEmail', 'Email', 'required');
		if ($this->form_validation->run() == TRUE)
		{
			$data = array('email' => $this->input->post('FEmail'));
			$res = $this->common->post_data('http://35.163.229.242/Master_trader/index.php/api/User/forget_password',$data);
			$this->common->processandredirect($res->status,$res->message,$res->message,'Login');
		} else {
			$this->session->set_flashdata('phperror','Please Enter Email Id To Retrive Your Password !');
			redirect(base_url('Login'));
		}
	}
	public function success()
	{
		//$this->session->set_flashdata('regsuccess','You Have Successfully Registerd');
		$this->session->set_flashdata('plan','You Have Successfully Upgrade The Plan');
		$this->load->view('success');
	}
	public function testtra()
	{
		$this->load->library('braintree_lib');
		$card = array(
		'number' => '5105105105105100',
		'cardholderName' => 'abc abc',
		'expirationDate' => '12/24',
		'cvv' => '123'
		);
		$result = $this->braintree_lib->transaction($card,'100');
		echo "<pre>"; print_r($result); exit;
	}
	function testplan()
	{//$this->session->userdata('Auth_Token')
		$where = array('auth_token' => '5446464','password' => '123');
		$res = $this->common->post_data('http://35.163.229.242/Master_trader/index.php/api/User/change_password',$where);
		echo "<pre>"; print_r($res); exit;
	}
}
/* End of file login.php */
/* Location: ./application/controllers/login.php */