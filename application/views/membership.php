<?php $this->load->view('include/header'); ?>
 <!-- Site content -->
 <!-- remove class class="setheader" -->
<div id="site-content" style="">
        <div class="container">
            <div class="row">
                <div class="page-title">
                    <h2 class="title text-center">Membership Options</h2>
                </div>                        
            </div><!-- /.row -->
        </div><!-- /.container -->
    <div id="page-body">

    
    <!-- membership -->
    <div class="flat-row pad-top60px pad-bottom60px bg-222222" style="margin-top: 1%;">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <img src="<?= base_url('assets/images/services/1.jpg') ?>" alt="images">
                </div><!-- /.col-md-4 -->
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="services-text-1">
                                <h3 style="color:lightgrey">DAILY TRADER</h3>
                                <p>
                                   <ul style="list-style-type:circle">
                                     <li style="color:lightgrey">At least 3 alerts during the day scalping 2% + (Not guaranteed, based on past trading history)</li>
                                     <li style="color:lightgrey">Unlimited chat-room access</li>
                                     <li style="color:lightgrey">Recommended for traders who want headache free quick returns within a short period of time (usually as short as few minutes)</li>
                                     <li style="color:lightgrey">Recommended Account Net Liquidity $50K + US Dollars</li>
                                     
                                   </ul>  
                                </p>
                            </div><!-- /.services-text-1 -->
                        </div><!-- /.col-md-8 -->
                        <div class="col-md-4">
                            <div class="pricing-table one-column">
                                <div class="price-column">
                                    <div class="column-container">
                                        <div class="plan">Pricing plans</div>
                                        <div class="price">
                                            <span class="symbol">$</span><span class="prices">149</span>
                                            <div class="ide">monthly</div>
                                            <div class="cta"><a class="button" href="#needtoscroll">Choose your plan</a></div>
                                        </div>
                                    </div>
                                </div><!-- /.price-column  -->
                            </div><!-- /.pricing-table -->
                        </div><!-- /.col-md-4 -->
                    </div><!-- /.row -->
                </div><!-- /.col-md-8 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.flat-row -->
    <div class="flat-row pad-top60px pad-bottom60px">
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <img src="<?= base_url('assets/images/services/2.jpg') ?>" alt="images">
                </div><!-- /.col-md-4 -->
                <div class="col-md-8">
                    <div class="row">
                        <div class="col-md-8">
                            <div class="services-text-1">
                                <h3>DAILY TRADER PRO</h3>
                                <p><ul style="list-style-type:circle">
                                     <li style="color:grey">Upto 30 alerts during the day. Average daily return 6% + (Not guaranteed, based on past trading history)</li>
                                     <li style="color:grey">Unlimited chat-room access</li>
                                     <li style="color:grey">Recommended for traders who enjoy trading back and forth and scalping anything that moves throughout the trading day</li>
                                     <li style="color:grey">Recommended Account Net Liquidity $150K + US Dollars</li>
                                     <li style="color:grey">BONUS!!! Daily hot/momentum stocks alert. High Risk/High Reward. Alerts are done by scanning and spotting the momentum stocks through various social media trading sites. please DO NOT invest more that 10% of your liquidity in those alerts as a rule of thumb cause of high risk</li>
                                   </ul> </p>
                            </div><!-- /.services-text-1 -->
                        </div><!-- /.col-md-8 -->
                        <div class="col-md-4">
                            <div class="pricing-table one-column">
                                <div class="price-column">
                                    <div class="column-container">
                                        <div class="plan">Pricing plans</div>
                                        <div class="price">
                                            <span class="symbol">$</span><span class="prices">299</span>
                                            <div class="ide">monthly</div>
                                            <div class="cta"><a class="button" href="#needtoscroll">Choose your plan</a></div>
                                        </div>
                                    </div>
                                </div><!-- /.price-column  -->
                            </div><!-- /.pricing-table -->
                        </div><!-- /.col-md-4 -->
                    </div><!-- /.row -->
                </div><!-- /.col-md-8 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.flat-row -->
        <!-- membership -->
        <div id="needtoscroll"></div>
<div class="row pad-top60px pad-bottom60px">
<div class="container">
    <div class="col-md-1"></div>
    <div class="col-md-5 text-center traderbox">
        <div class="imagebox imagebox-trader">
            <div class="box-wrapper box-wrapper-trader">
                <div class="box-title-trader text-center" style="background-color: #3C3C3C;">
                    <h5 style="color:#ffffff;"><?= $plan_data->monthly[0]->plan_name ?></h5>
                </div>
                <?php foreach ($plan_data as $key => $value) { ?>
                <div class="box-content-part1 well text-center">
                    <label class="trader-price">$<?= $value[0]->amount ?></label><br>
                    <label class="trader-plan"><?= str_replace('_', ' ', $value[0]->plan_type).' '. $value[0]->plan_discount ?></label><br>
                    <a href="<?= base_url('register/').base64_encode($value[0]->id) ?>" class="btn btn-success btn-md marbtm" title="">Register</a>
                </div>
            <?php } ?>
            </div><!-- /.box-wrapper -->
        </div><!-- /.imagebox   -->
    </div><!-- /.col-md-3 -->
    <!-- <div class="col-md-1"></div> -->
    <div class="col-md-5 text-center col-md-offset-1 traderbox ">
        <div class="imagebox imagebox-trader">
            <div class="box-wrapper box-wrapper-trader">
                <div class="box-title-trader text-center" style="background-color: #3C3C3C;">
                    <h5 style="color:#ffffff;"><?= $plan_data->monthly[1]->plan_name ?></h5>
                </div>
                <?php foreach ($plan_data as $key => $value) { ?>
                <div class="box-content-part1 well text-center">
                    <label class="trader-price">$<?= $value[1]->amount ?></label><br>
                    <label class="trader-plan"><?= str_replace('_', ' ', $value[1]->plan_type. ' '. $value[1]->plan_discount) ?></label><br>
                    <a href="<?= base_url('register/').base64_encode($value[1]->id) ?>" class="btn btn-success btn-md marbtm" title="">Register</a>
                </div>
                <?php } ?>
            </div><!-- /.box-wrapper -->
        </div><!-- /.imagebox   -->
    </div><!-- /.col-md-3 -->
    </div> 
</div>
    <div class="flat-row bg-f4f4f4 pad-top60px pad-bottom60px">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="iconbox">
                        <div class="box-header">
                            <div class="box-icon">
                                <i class="fa icons icon-calculator"></i>
                            </div>
                            <h5 class="box-title">Management Support</h5>
                        </div>
                        <div class="box-content">
                            <span class="font-size-14px">The professional management of a business doesn’t live without accurate information and timely.</span>
                        </div>
                    </div><!-- /.iconbox -->
                </div><!-- /.col-md-3 -->
                <div class="col-md-3">
                    <div class="iconbox">
                        <div class="box-header">
                            <div class="box-icon">
                                <i class="fa icons icon-layers"></i>
                            </div>
                            <h5 class="box-title">Investment Projects</h5>
                        </div>
                        <div class="box-content">
                            <span class="font-size-14px">For those looking to invest in the<br>growth of your business or create a new business.</span>                                        
                        </div>
                    </div><!-- /.iconbox -->
                </div><!-- /.col-md-3 -->
                <div class="col-md-3">
                    <div class="iconbox ">
                        <div class="box-header">
                            <div class="box-icon">
                                <i class="fa icons icon-globe"></i>
                            </div>
                            <h5 class="box-title">New Entrepreneurs</h5>
                        </div>
                        <div class="box-content">
                            <span class="font-size-14px">Evaluate your business idea.<br>Without any associated cost. For new entrepreneurs.</span>                 
                        </div>
                    </div><!-- /.iconbox -->
                </div><!-- /.col-md-3 -->
                <div class="col-md-3">
                    <div class="iconbox ">
                        <div class="box-header">
                            <div class="box-icon">
                                <i class="fa icons icon-chart"></i>
                            </div>
                            <h5 class="box-title">Restructuring Plans</h5>
                        </div>
                        <div class="box-content">
                            <span class="font-size-14px">Adapt your business to the new market conditions. Redefine your strategy, restructure your debt.</span>                                        
                        </div>
                    </div><!-- /.iconbox -->
                </div><!-- /.col-md-3 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.flat-row -->
 </div><!-- /.page-body -->
</div><!-- /#site-content -->       <!-- Second Section -->
<?php $this->load->view('include/footer'); ?>