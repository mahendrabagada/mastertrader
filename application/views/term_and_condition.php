<?php $this->load->view('include/header'); ?>
    <div class="box">
	<!-- Site content -->
        <div id="site-content" style="padding-top:5%">                    
            <section class="flat-row">
                <div class="container">
                    <div class="row">
                        <h3>Terms & conditions</h3>
                        <p>
                            You agree to indemnify and hold Mastertraderalerts.com, its subsidiaries, and affiliates, and their respective officers, agents, partners and employees, harmless from any loss, liability, claim, or demand, including reasonable attorneys’ fees, made by any third party due to or arising out of your use of this Site in violation of these Terms of Use and/or arising from a breach of these Terms of Use and/or any breach of your representations and warranties set forth above and/or if any material that you post using this Site causes us to be liable to another. We reserve the right to defend any such claim, and you agree to provide us with such reasonable cooperation and information as we may request.
                        </p>
                        <p>
                            We reserve the right at any time and from time-to-time to modify, edit, delete, suspend or discontinue, temporarily or permanently this Site (or any portion thereof) and/or the information, materials, products and/or services available through this Site (or any part thereof) with or without notice. You agree that we shall not be liable to you or to any third party for any such modification, editing, deletion, suspension or discontinuance of this Site.
                        </p>
                        <p>
                            <a href="#">Mastertraderalerts.com</a> has the right to cancel the membership of any site user for any reason at any time, without refunding or reimbursing the membership fee to the site user.
                        </p>
                        <h5>
                            <strong>
                                <u>
                                    Terms of Subscription Renewal
                                </u>
                            </strong>
                        </h5><br>
                        <h5>
                            <strong>
                               Trial-Period Offers
                               
                            </strong>
                        </h5>
                        <p>
                            If you are taking part in any trial period offer, you must cancel the service by the end of the trial period to avoid incurring new charges unless we notify you otherwise. If you do not cancel the service prior to the end of the trial period, you authorize us to charge your payment method for the service. <strong>Note: At the current time MasterTraderAlerts.com does NOT offer any free trials.</strong>
                        </p>


                        <h5>
                            <strong>
                                Monthly Subscription:
                            </strong>
                        </h5>
                        <p>
                            Mastertraderalerts’ Monthly Subscriptions automatically renew each month until the subscription buyer notifies the company by email that they wish to cancel.
                        </p>
                        <h5>
                            <strong>
                                Quarterly Subscription:
                            </strong>
                        </h5>
                        <p>
                            MasterTraderAlerts’ Quarterly Subscriptions automatically renew each quarter until the subscription buyer notifies the company by email that they wish to cancel or convert to a different subscription payment option.
                        </p>
                        <h5>
                            <strong>
                                Semi-Annual Subscription:
                            </strong>
                        </h5>
                        <p>
                            Prior to subscription expiration, if the subscriber does not notify the company that they wish to continue at the stated Subscription payment option, the subscriber’s account will revert to the monthly payment option automatically, and will be charged accordingly.
                        </p>
                        <h5>
                            <strong>
                                <u>
                                    Refund/Cancellation Policy/Terms
                                </u>
                            </strong>
                        </h5>
                        <h5>
                            <strong>
                                Monthly  subscribers:  
                            </strong>
                        </h5>
                        <p>
                            After payment has been received, <strong>no refund</strong> will be provided if cancelled within the monthly period. However, alert services will still be provided to you for the remainder of the monthly cycle.
                        </p>
                        <h5>
                            <strong>
                                Quarterly subscribers:  
                            </strong>
                        </h5>
                        <p>
                            If you choose to cancel your quarterly subscription at any time period within 3 months, your charges will be rounded up to the following month and prorated by the amount of time remaining within the original 3 month subscription period. Cancellations within the first 30 days will result in only a 2-month subscription service charge refund.
                        </p>
                        <h5>
                            <strong>
                                Semi-Annual Subscribers:  
                            </strong>
                        </h5>
                        <p>
                            If you choose to cancel your semi- annual subscription at any time period within 6 months, your charges will be rounded up to the start of the following month and prorated refund will be issued by the amount of time remaining within the original 6 month subscription period. Cancellations within the first 30 days will result in only a 5-month subscription service charge refund.
                        </p>
                        <p>
                            <strong>NOTE:</strong> If the credit card associated with your membership service should expire, terminate or any payment is otherwise rejected by the issuing company, MasterTraderAlerts may immediately terminate your membership service. It is solely your responsibility to ensure:<br>
                            (1) That valid credit card information remains on file for your account or service and the automatic renewal thereof, and <br>
                            (2) That a valid email address remains on file for your membership service for any communications from MasterTraderAlerts related thereto.
                        </p>
                        <p>
                            The cancellation of your account or service will immediately result in deactivation and deletion of your account or service, the denial of access to the Site and the forfeiture and relinquishment of all content and information within or related to your account or service. MasterTraderAlerts may retain data, content or information from your account after cancellation in backup and/or archival copies of the Site and related databases, but such copies, if any, will not be available to you.
                        </p>
                        <p>
                            <strong>NOTE:</strong> MasterTraderAlerts is a Wholly Owned Subsidiary of UNLIMITED ADVERTISERS INC. All payments made through PayPal Merchant Services will be charged by UNLIMITED ADVERTISERS INC..
                        </p>

                    </div>    
                </div>
            </section>               
           

            <div id="page-body">
                <div class="flat-row">
                    <div class="container">
                        <div class="row">
                            
                        </div><!-- /.row -->
                    </div><!-- /.container -->
                </div><!-- /.flat-row -->

                <div class="flat-row">
                    <div class="container">
                        <div class="row">
                            
                        </div><!-- /.row -->
                    </div><!-- /.container -->
                </div><!-- /.flat-row -->
                
          



              

                
            </div><!-- /.page-body -->
        </div><!-- /#site-content -->
    </div>

<?php $this->load->view('include/footer'); ?>