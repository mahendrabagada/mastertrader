<?php $active = 'cal'; ?>
<?php $this->load->view('include/header'); ?>
<?php $this->load->view('include/message'); ?>
 <!-- Site content -->

  <style type="text/css">
table.calc-table th,td {
    max-width: 100px;
    padding: 0.857em 0.587em;
}   
table.calc-table {
    border-collapse: collapse;
    text-align: center;
    width: 100%;
}
table.calc-table, table.calc-table td, table.calc-table th {
    border: 1px solid #bbb;
}
table.calc-table {
    border-collapse: collapse;
    text-align: center;
    width: 100%;
}
table.calc-table, table.calc-table td, table.calc-table th {
    border: 1px solid #bbb !important;
}
table {
    border-collapse: collapse;
    border-spacing: 0;
}
table.calc-table thead tr {
    background: #E4E4E4;
    color: #0071bd;
    text-align: center !important;
}
table.calc-table thead tr th {
    font-weight:bold;
    text-align: center !important;
}
table.calc-table tbody tr td {
    word-break: break-all;
}
p{
    text-align: justify;
}
.btn.btn-show-table {
    display: block;
    font-size: 18px;
    margin: 15px;
    text-align: center;
}
a {
    color: #0071bd;
}
select{
    background: rgba(0, 0, 0, 0.05) none repeat scroll 0 0;
    border: 1px solid rgba(0, 0, 0, 0.06);
    box-sizing: border-box;
    height: 30px;
    max-width: 100%;
    padding: 0px;
    position: relative;
    transition: all 0.2s ease-in-out 0s;
}
.final-output ul {
     line-height: 26px;
    list-style-type: disc;
    padding: 0 0 23px 16px;
}

.result-list li {
    font-size: 17px;
}
.result-list span {
    font-weight: bold;
}
.col-md-3,.col-md-9{
    padding-left:0px !important;
}

  </style>
  <div class="container"> 
    <div id="site-content" style="margin-top:8%">
    <div id="page-body">
    <div>
    <div class="container">
    <div class="row">                            
        <h3 class="margin-top20px">The Miracle of Compound Interest</h3>                           
    </div><!-- /.row -->
    <div class="flat-divider d20px"></div>
    <div class="row">
    <div class="entry-content">
            <p>Albert Einstein once said: <b>“Compound interest is the eighth wonder of the world. He who understands it earns it… he who doesn’t… pays it.”</b><br>
    Here are similar sayings you heard many, many times:
    Life is really simple, but we insist on making it complicated” or “Life is only as complicated as we make it”.<br><br>
    Here at MasterTraderAlerts we apply and realize the above-mentioned concept towards trading Equities, ETF’s, and Options (rarely) compounding our assets by a small percentage daily gains that eventually creates humongous wealth.
    In calculator below, please insert the dollar amount in you are trading with, add the percentage number daily (our average, but not guaranteed is 2%-3%) and see the result in a year (261 trading days). Also, note that compound interest returns the maximum benefits when you do not take any profits out of the account (recommended reinvest rate 100%)</p>
            <p>&nbsp;</p>
        </div>
        <div>
            <p>In calculator below, please insert the dollar amount in you are trading with, add the percentage number daily (our average, but not guaranteed is 2%-3%) and see the result in a year (261 trading days). Also, note that compound interest returns the maximum benefits when you do not take any profits out of the account (recommended reinvest rate 100%).<br>
    Charts below shows few examples of how much the initial dollar amount becomes after certain period of time with certain percentage of daily compound interest accumulation. (assuming no withdrawal at any time till the period ends.)</p>
        </div>
     <div class="flat-divider d40px"></div>
    <div class="row">
        <div class="col-md-6">

            <p><strong>Example 1.</strong><br>
            Time period of <strong>261 days,</strong> which represents stock market open days in <strong>One</strong> calendar <strong>year</strong>.</p>
            <table class="calc-table">
            <thead>
            <tr>
                <th>Initial Dollar<br>
                Amount</th>
                <th>Daily Return<br>
                1%</th>
                <th>Daily Return<br>
                2%</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>$25,000</td>
                <td><strong>$335,597.59</strong></td>
                <td><strong>$4,391,214.71</strong></td>
            </tr>
            <tr>
                <td>$50,000</td>
                <td><strong>$671,194.34</strong></td>
                <td><strong>$8,782,435.47</strong></td>
            </tr>
            <tr>
                <td>$75,000</td>
                <td><strong>$1,006,792.34</strong></td>
                <td><strong>$13,173,650.31</strong></td>
            </tr>
            <tr>
                <td>$100,000</td>
                <td><strong>$1,342,389.60</strong></td>
                <td><strong>$17,564,865.40</strong></td>
            </tr>
            </tbody>
            </table> 
        </div>
        <div class="col-md-6">
            
            <p><strong>Example 2.</strong><br>
            Time period of <strong>522 days,</strong> which represents stock market open days in <strong>Two calendar years.</strong></p>
            <table class="calc-table">
            <thead>
            <tr>
                <th>Initial Dollar<br>
                Amount</th>
                <th>Daily Return<br>
                1%</th>
                <th>Daily Return<br>
                2%</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>$25,000</td>
                <td><strong>$4,505,027.20</strong></td>
                <td><strong>$771,310,958.25</strong></td>
            </tr>
            <tr>
                <td>$50,000</td>
                <td><strong>$9,010,042.10</strong></td>
                <td><strong>$1,542,622,988.44</strong></td>
            </tr>
            <tr>
                <td>$75,000</td>
                <td><strong>$13,515,075.23</strong></td>
                <td><strong>$2,313,933,969.34</strong></td>
            </tr>
            <tr>
                <td>$100,000</td>
                <td><strong>$18,020,097.65</strong></td>
                <td><strong>$3,085,244,992.84</strong></td>
            </tr>
            </tbody>
            </table>

        </div>
        
    </div>
    <div class="row">
    <div class="title-section">
            <h3 class="flat-row bg-222222" style="color:white">Calculate Your Daily Interest for a Fixed Amount of Days</h3>
        </div><!-- /.title-section -->
    </div><!-- /.row -->


          
    </div><!-- /.row -->                        
    </div><!-- /.flat-row -->
    </div><!-- /.page-body -->
        </div><!-- /#site-content -->
        <?php
  /*  Calculate Data */

$initial_amount = !empty($_POST['initial_amount']) ? $_POST['initial_amount'] : 5000;
$interest_rate = !empty($_POST['interest_rate']) ? $_POST['interest_rate'] : .25;
$term_length = !empty($_POST['length_of_terms']) ? $_POST['length_of_terms'] : 5;
$reinvest_rate = !empty($_POST['reinvest_rate']) ? $_POST['reinvest_rate'] : 100;
$exclude_weekends = !empty($_POST['exclude_weekends']) ? $_POST['exclude_weekends'] : 'yes';
$new_length = $term_length;
?>
</div>
<div class="container">

                <form class="calculator-form" id="calculator-form" action="#" method="POST">
                    <div class="row">
                        <div class="form-group  col-md-3">
                              <labe for="initial_amount">Initial Purchase Amount ($)</label>
                        </div>
                        <div class="form-group col-md-3">
                            <input style="width:140px; height: 35px;" type="text" name="initial_amount" id="initial_amount" class="form-control" value="<?php echo $initial_amount;?>">
                        </div>
                      
                        <div class="form-group  col-md-3">
                              <label for="interest_rate">Daily Interest Rate (%)</label>
                        </div>
                        <div class="form-group col-md-3">
                            <input  style="width:140px; height: 35px;" type="text" name="interest_rate" id="interest_rate" class="form-control" value="<?php echo $interest_rate;?>">
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group  col-md-3">
                             <label for="length_of_terms">Length of Term (in days)</label>
                        </div>
                        <div class="form-group col-md-3">
                            <input style="width:140px; height: 35px;" type="text" name="length_of_terms" id="length_of_terms" class="form-control" value="<?php echo $term_length;?>">
                        </div>

                        <div class="form-group  col-md-3">
                             <label for="reinvest_rate">Daily Reinvest Rate (%)</label>
                        </div>
                        <div class="form-group  col-md-3">
                            <select style="width:140px; height: 35px;" name="reinvest_rate" id="reinvest_rate">
                                <?php //for( $i=0; $i<=100; $i+=5 ){
                                    //echo '<option value="'.$i.'">'.$i.'</option>';
                                //}?>
                                <option value="80" <?php echo ('80' == $reinvest_rate) ? 'selected="selected"':'' ?>>80</option>
                                <option value="90" <?php echo ('90' == $reinvest_rate) ? 'selected="selected"':'' ?>>90</option>
                                <option value="100" <?php echo ('100' == $reinvest_rate) ? 'selected="selected"':'' ?>>100</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group  col-md-3">
                              <label for="exclude_weekends">Exclude Weekends</label>
                        </div>
                        <div class="form-group  col-md-9"> 
                            <select  style="width:140px; height: 35px;" name="exclude_weekends" id="exclude_weekends">
                                <option value="yes" <?php echo ('yes' == $exclude_weekends) ? 'selected="selected"':'' ?>>Yes</option>
                                <option value="no" <?php echo ('no' == $exclude_weekends) ? 'selected="selected"':'' ?>>No</option>
                            </select>
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group  col-md-3">
                              <button type="reset" style="width:140px;background-color: #fff;border-radius:0px;font-weight:bold;color:#0071BD" value="Reset" class="btn btn-primary pull-left">Reset </button>
                        </div>

                        <div class="form-group  col-md-3">
                                <input type="submit" name="submit" value="Calculate" class="btn btn-primary calculate">                            
                        </div>
                    </div>
                </form>
                
                 <div class="display-final-result"> 
                    
                  </div>
                 
                <a class="btn btn-show-table" href="javascript:void(0);"> Show/Hide Calculator </a>             
              <div class="content hidecl">

                <?php if(isset($_POST['submit'])){?>
                
                
                    <table class="table-results calc-table">
                        <thead>
                            <tr>
                            <th>Day</th>
                            <th>Date</th>
                            <th>Earnings</th>
                            <th>Reinvest</th>
                            <th>Gain</th>
                            <th>Cash Out</th>
                            <th>Total Principal</th>
                            <th>Total Cash</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php
                        
                            function price_format( $price )
{
    return number_format($price,'2','.',',');
}
                                
                        // Tue, Jul 19, 2016
                        $Today = date('D, M j, Y');
                        // echo ' today is ' . $Today;

                        // echo 'tomorrow is '. $tomorrow->format('D, M j, Y'); // 2010-12-25
                        $principal = $_POST['initial_amount'];
                        $invest_rate = $_POST['interest_rate'];
                        $total_days = $_POST['length_of_terms'];
                        // echo 'totall days ' . $total_days;
                        $daily_reinvest = $_POST['reinvest_rate'];
                        $daily_reinvest_rate = $daily_reinvest/100;
                        $totalcash = 0;
                        $count = 1;
                        for( $i = 1; $i <= $total_days; $i++ ){
                            if($i == 1){
                                $tomorrow = new DateTime($Today.' + 1 day');
                            }
                            else{
                                $tomorrow = new DateTime($Today.' + '.$i.' days');
                            }

                            // $Today = $tomorrow; exclude_weekends
                            if( (($tomorrow->format('D') == 'Sat') || ($tomorrow->format('D') == 'Sun')) && ($_POST['exclude_weekends'] != 'no') ){
                                $total_days++;
                                ?>
                                <tr>
                                    <td></td>
                                    <td><?php echo $tomorrow->format('D, M j, Y') ;?></td>
                                    <td colspan="6">[ Weekend ]</td>
                                </tr>
                            <?php }
                            else{
                                $earnings = round(($principal*$invest_rate)/100, 2);
                                $new_principal = round($earnings*$daily_reinvest_rate, 2);
                                $cashout = $earnings-$new_principal;
                                $principal = $principal+$new_principal;
                                $totalcash += $cashout;
                                
                            ?>
                            <tr>
                                <td><?php echo $count ;?></td>
                                <td><?php echo $tomorrow->format('D, M j, Y') ;?></td>
                                <td>$ <?php echo price_format($earnings) ;?></td>
                                <td><?php echo price_format($daily_reinvest) ;?> %</td>
                                <td>$ <?php echo price_format($new_principal) ;?></td>
                                <td>$ <?php echo price_format($cashout) ;?></td>
                                <td>$ <?php echo price_format($principal) ;?></td>
                                <td>$ <?php echo price_format($totalcash) ;?></td>
                            </tr>
                            <?php $count++;
                            } ?>
                        <?php } ?>
                        </tbody>
                    </table>           
                         
                    <div class="final-output">
                        <ul class="result-list" style="list-style-type:disc">
                            <li>You started with an investment of: <span>$<?php echo price_format($_POST['initial_amount']);?></span> on <?php echo $Today; ?></li>
                            <li>Your principal amount grew to: <span>$<?php echo price_format($principal);?></span> by <?php echo $tomorrow->format('D, M j, Y') ;?></li>
                            <li>Your total cash withdrawals were: <span>$<?php echo price_format($totalcash);?></span> over the course of <span><?php echo $_POST['length_of_terms'];?></span> days </li>
                            <?php
                                $total_profit = ($principal + $totalcash) - $_POST['initial_amount'];
                                $profit_percent = $total_profit/$_POST['initial_amount']*100;
                            ?>
                            <li>Your total NET profit for the <?php echo $_POST['length_of_terms'];?>-day period was <span>$<?php echo price_format($total_profit); ?><span> ( <span style="font-weight: bold; color: #60bd57"><?php echo price_format($profit_percent, 2); ?>% </span>) </li>
                        </ul>
                </div>
               
                <?php } ?>
           
          <!-- <p><a href="!#" class="back-to-calc btn btn-secondary">Back To Calculator</a></p> -->
    
    </div>
    </div>
<?php $this->load->view('include/footer'); ?>
<script type="text/javascript">
     $(document).ready(function($) {
        var str = $( ".final-output" ).contents();
        $( ".display-final-result" ).html(str);
        
     });

    $(document).ready(function() {
        $('.btn-show-table').click(function(event) {
            $('.hidecl').toggle();
        });
        
    });
</script>
<!--Start of Zendesk Chat Script-->
<script type="text/javascript">
window.$zopim||(function(d,s){var z=$zopim=function(c){z._.push(c)},$=z.s=
d.createElement(s),e=d.getElementsByTagName(s)[0];z.set=function(o){z.set.
_.push(o)};z._=[];z.set._=[];$.async=!0;$.setAttribute("charset","utf-8");
$.src="https://v2.zopim.com/?4UFIG0F98z2wobynPSbOiHCvpgkPIBbp";z.t=+new Date;$.
type="text/javascript";e.parentNode.insertBefore($,e)})(document,"script");
</script>
<!--End of Zendesk Chat Script-->
