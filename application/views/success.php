<?php $this->load->view('include/header'); ?>
<style type="text/css" media="screen">
 h2 { margin: 113px 0px 20px; color: green; }
</style>
<div id="site-content" >
    <div id="page-body">
        <div class="flat-row bg-f7f7f7 pad-top0px pad-bottom0px">
            <div class="container">
                <div class="row">
                    <div class="col-md-12 col-lg-12 col-sm-12">
                    <?php if( $regsuccess = $this->session->flashdata('regsuccess')): ?>
                    <h2 class="text-center"><?= $regsuccess ?>...</h2>
                    <h2 class="text-center">Thanks For Joining Us........</h2>
                    <?php endif;?>
                    <?php if( $plan = $this->session->flashdata('plan')): ?>
                    <h2 class="text-center"><?= $plan ?>...</h2>
                    <?php endif;?>
                            <div class="flat-divider d20px"></div>
                    </div><!-- /.col-md-6 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </div><!-- /.flat-row -->
    </div>
</div>
<?php $this->load->view('include/footer'); ?>