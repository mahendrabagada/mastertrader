<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US"><!--<![endif]-->
    <head>
        <!-- Basic Page Needs -->
        <meta charset="utf-8">
            <!--[if IE]><meta http-equiv='X-UA-Compatible' content='IE=edge,chrome=1'><![endif]-->
            <title>Master Trader | Daily Trading Alerts and Unlimited Chat</title>

            <meta name="author" content="themesflat.com">

                <!-- Mobile Specific Metas -->
                <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
                    <!-- Bootstrap  -->
                    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/stylesheets/bootstrap.css') ?>" >

                        <!-- Theme Style -->
                        <link rel="stylesheet" type="text/css" href="<?= base_url('assets/stylesheets/style.css') ?>">
                            <link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/custom.css') ?>">

                                <!-- Responsive -->
                                <link rel="stylesheet" type="text/css" href="<?= base_url('assets/stylesheets/responsive.css') ?>">

                                    <!-- Colors -->
                                    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/stylesheets/colors/color1.css') ?>" id="colors">

                                        <!-- Animation Style -->
                                        <link rel="stylesheet" type="text/css" href="<?= base_url('assets/stylesheets/animate.css') ?>">
                                            <link rel="stylesheet" href="<?= base_url('assets/stylesheets/sweetalert.css') ?>">

                                                <!-- Favicon and touch icons  -->
                                                <link href="icon/apple-touch-icon-48-precomposed.png" rel="apple-touch-icon-precomposed" sizes="48x48">
                                                    <link href="icon/apple-touch-icon-32-precomposed.png" rel="apple-touch-icon-precomposed">
                                                        <link href="icon/favicon.png" rel="shortcut icon">
                                                            <style>

                                                            </style>
                                                            </head>
                                                            <body class="header-sticky page-loading">   
                                                                <div class="loading-overlay">
                                                                </div>
                                                                <!-- Boxed -->
                                                                <div class="boxed">
                                                                    <div id="site-header">
                                                                        <div class="flat-top">
                                                                            <div class="container">
                                                                                <div class="row">
                                                                                    <div class="flat-wrapper">
                                                                                        <div class="custom-info">
                                                                                            <span>Have any questions?</span>
                                                                                            <span><i class="fa fa-envelope"></i><a href="mailto:support@mastertraderalerts.com" style="color:lightgrey">support@mastertraderalerts.com</a></span>
                                                                                        </div>
                                                                                    </div><!-- /.flat-wrapper -->
                                                                                    <!-- <?php
                                                                                    //if($this->session->userdata('UserName') != ''){       $getnotification = $this->common->post_data('http://35.163.229.242/Master_trader/index.php/api/User/get_alerts',array('auth_token' => $this->session->userdata('AuthToken'),'page_nu'=>'0'));
                                                                                    //$alert = $getnotification->alerts_data;
                                                                                    ?> -->
                                                                                    <?php
                                                                                    if ($this->session->userdata('UserName') != '') {

                                                                                        $getnotification = $this->common->post_data('http://35.163.229.242/Master_trader/index.php/api/User/get_alerts', array('auth_token' => $this->session->userdata('AuthToken'), 'page_nu' => '0'));
                                                                                        $alert = array();
                                                                                        if ($getnotification->status != 0) {
                                                                                            $alert = $getnotification->alerts_data;
                                                                                        } else {
                                                                                            $alert = array();
                                                                                        }
                                                                                        ?>
                                                                                        <style type="text/css">
                                                                                          
                                                                                            .label {
                                                                                                background-color:#E66454;
                                                                                                display: block;
                                                                                                font-size: 8px;
                                                                                                line-height: 10px;
                                                                                                padding: 1px 4px;
                                                                                                position: absolute;
                                                                                                right: 7px;
                                                                                                top: 0;
                                                                                                border-radius: 0.25em;
                                                                                                color: #fff;
                                                                                                display: inline;
                                                                                                font-size: 75%;
                                                                                                font-weight: bold;
                                                                                                line-height: 1;
                                                                                                text-align: center;
                                                                                                vertical-align: baseline;
                                                                                                white-space: nowrap;
                                                                                            }
                                                                                            .hover_color:hover{
                                                                                                background-color: white;
                                                                                                cursor: pointer;
                                                                                            }
                                                                                        </style>
                                                                                        <div class="col-md-3 pull-right">
                                                                                            <div class="row pull-right">
                                                                                                <div class="col-xs-6 hover_color">

                                                                                                    <div >
                                                                                                        <span class="label">5</span><i class="nav-icon fa fa-bullhorn" style="color:#0A5BB9;" data-toggle="dropdown" ></i>

                                                                                                        <ul class="dropdown-menu scroll-menu scroll-menu-2x" role="menu" aria-labelledby="menu1" style="width: 300px;margin-left: -246px !important">
                                                                                                            <div class="slimScrollDiv container" style="position: relative; overflow: auto; width: auto; height: 250px;">

                                                                                                                <?php foreach ($alert as $value) { ?>

                                                                                                                    <!-- <li role="presentation" class="dropdown-header" style="color: 
                                                                                                                        #DAA520;"><?= $value->symbol ?></li> -->
                                                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#" style="color: #DAA520;"><?= $value->symbol ?></a></li>

                                                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#" style="color: red;"><?= $value->type ?></a></li>

                                                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#"><b><?= $value->price ?></b></a></li>

                                                                                                                    <li role="presentation"><a role="menuitem" tabindex="-1" href="#" style="color: black;"><?= $value->comment ?></a></li>

                                                                                                                    <li role="presentation" class="divider"></li>


                                                                                                                <?php } ?>
                                                                                                            </div>
                                                                                                        </ul>

                                                                                                    </div>

                                                                                                </div>

                                                                                                <div class="col-xs-6 hover_color">
                                                                                                    <ul>
                                                                                                        <li>
                                                                                                            <div >
                                                                                                                <span class="label">5</span><i class="nav-icon fa fa-envelope dropdown-toggle" style="color:#0A5BB9" data-toggle="dropdown" ></i>

                                                                                                                <ul class="dropdown-menu scroll-menu scroll-menu-2x" role="menu" aria-labelledby="menu1" style="width: 300px;margin-left: -246px !important">
                                                                                                                    <div class="slimScrollDiv container" style="position: relative; overflow: auto; width: auto; height: 250px;">

                                                                                                                        <?php foreach ($alert as $value) { ?>

                                                                                                                            <!-- <li role="presentation" class="dropdown-header" style="color: 
                                                                                                                                #DAA520;"><?= $value->symbol ?></li> -->
                                                                                                                            <li role="presentation"><a role="menuitem" tabindex="-1" href="#" style="color: #DAA520;"><?= $value->symbol ?></a></li>

                                                                                                                            <li role="presentation"><a role="menuitem" tabindex="-1" href="#" style="color: red;"><?= $value->type ?></a></li>

                                                                                                                            <li role="presentation"><a role="menuitem" tabindex="-1" href="#"><b><?= $value->price ?></b></a></li>

                                                                                                                            <li role="presentation"><a role="menuitem" tabindex="-1" href="#" style="color: black;"><?= $value->comment ?></a></li>

                                                                                                                            <li role="presentation" class="divider"></li>


                                                                                                                        <?php } ?>
                                                                                                                    </div>
                                                                                                                </ul>

                                                                                                            </div>
                                                                                                        </li>
                                                                                                    </ul>
                                                                                                </div>

                                                                                            </div>
                                                                                        </div>
                                                                                    <?php } ?>
                                                                                </div><!-- /.row -->
                                                                            </div><!-- /.container -->
                                                                        </div><!-- /.flat-top -->

                                                                        <header id="header" class="header clearfix"> 
                                                                            <div class="header-wrap clearfix header-style02">
                                                                                <div class="container">
                                                                                    <div class="row">
                                                                                        <div class="flat-wrapper">
                                                                                            <div id="logo" class="logo">
                                                                                                <a href="<?= base_url() ?>">
                                                                                                    <img src="<?= base_url('assets/images/logo.png') ?>" alt="images">
                                                                                                </a>
                                                                                            </div><!-- /.logo -->
                                                                                            <div class="btn-menu">
                                                                                                <span></span>
                                                                                            </div><!-- //mobile menu button -->
                                                                                            <div class="nav-wrap">        
                                                                                                <nav id="mainnav" class="mainnav"> 

                                                                                                    <?php if ($this->session->userdata('UserName') == '') { ?>
                                                                                                        <div class="menu-extra">
                                                                                                            <ul>
                                                                                                                <li>
                                                                                                                    <a href="<?= base_url('Login') ?>">
                                                                                                                        <i class="fa fa-user"></i>
                                                                                                                    </a>  
                                                                                                                </li>
                                                                                                            </ul>
                                                                                                        </div> <!-- /.menu-extra-->
                                                                                                    <?php
                                                                                                    } else {
                                                                                                        $disname = !empty($this->session->userdata('UserName')) ? $this->session->userdata('UserName') : 'Profile';
                                                                                                        ?>
                                                                                                        <ul>
                                                                                                            <li><a class="dropdown-toggle m" data-toggle="dropdown" style="font-weight: normal;"><i class="fa fa-user"></i>&nbsp;<?= $disname ?></a>
                                                                                                                <ul class="dropdown-menu pad">
                                                                                                                    <li><a href="<?= base_url('Profile') ?>"><i class="fa fa-user fa-lg"></i> My Profile</a></li>
                                                                                                                    <li><a href="<?= base_url('Login/Logout') ?>"><i class="fa fa-sign-out fa-lg"></i> Log out</a></li>
                                                                                                                </ul>
                                                                                                            </li>
                                                                                                        </ul>
                                                                                                    <?php } ?>

                                                                                                    <ul class="menu">
                                                                                                        <li <?= 'class="' . $this->common->active_class('Home') . '"' ?>>
                                                                                                            <a href="<?= base_url() ?>">Home</a>
                                                                                                        </li>
                                                                                                        <li <?= 'class="' . $this->common->active_class('Membership') . '"' ?>><a href="<?= base_url('Membership/member') ?>">Membership</a></li>
                                                                                                        <li <?= 'class="' . $this->common->active_class('Affiliate') . '"' ?>><a href="<?= base_url('Affiliate') ?>">Affiliate</a></li>
                                                                                                        <li  <?= 'class="' . $this->common->active_class('Calculate') . '"' ?>> <a href="<?= base_url('Calculate') ?>">Compound Interest</a></li>
                                                                                                        <li  <?= 'class="' . $this->common->active_class('About') . '"' ?>> <a href="<?= base_url('About/abouts') ?>">About MTA</a></li>
                                                                                                        <li  <?= 'class="' . $this->common->active_class('Contact') . '"' ?>><a href="<?= base_url('Contact') ?>">Contact</a></li>
                                                                                                    </ul><!-- /.menu -->
                                                                                                </nav><!-- /.mainnav -->  
                                                                                            </div><!-- /.nav-wrap -->
                                                                                        </div><!-- /.flat-wrapper -->
                                                                                    </div><!-- /.row -->
                                                                                </div><!-- /.container -->
                                                                            </div><!-- /.header-inner --> 
                                                                        </header><!-- /.header -->
                                                                    </div><!-- /.site-header -->