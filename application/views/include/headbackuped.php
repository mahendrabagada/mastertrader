<!DOCTYPE html><?php error_reporting(0); ?>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en-US" lang="en-US">
<head>
    <meta charset="utf-8">
    <title>Master Trader | Daily Trading Alerts and Unlimited Chat</title>
    <meta name="author" content="themesflat.com">
    <!-- Mobile Specific Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <!-- Bootstrap  -->
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/stylesheets/bootstrap.css') ?>" >
    <!-- Theme Style -->
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/stylesheets/style.css') ?>">
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/custom.css') ?>">
    <!-- Responsive -->
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/stylesheets/responsive.css') ?>">
    <!-- Colors -->
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/stylesheets/colors/color1.css') ?>" id="colors">
    <!-- Animation Style -->
    <link rel="stylesheet" type="text/css" href="<?= base_url('assets/stylesheets/animate.css') ?>">
    <link rel="stylesheet" href="<?= base_url('assets/stylesheets/sweetalert.css') ?>">
    <!--  Favicon and touch icons -->
    <link rel="apple-touch-icon" sizes="57x57" href="<?= base_url('assets/images/favicon/apple-icon-57x57.png') ?>">
    <link rel="apple-touch-icon" sizes="60x60" href="<?= base_url('assets/images/favicon/apple-icon-60x60.png') ?>">
    <link rel="apple-touch-icon" sizes="72x72" href="<?= base_url('assets/images/favicon/apple-icon-72x72.png') ?>">
    <link rel="apple-touch-icon" sizes="76x76" href="<?= base_url('assets/images/favicon/apple-icon-76x76.png') ?>">
    <link rel="apple-touch-icon" sizes="114x114" href="<?= base_url('assets/images/favicon/apple-icon-114x114.png') ?>">
    <link rel="apple-touch-icon" sizes="120x120" href="<?= base_url('assets/images/favicon/apple-icon-120x120.png') ?>">
    <link rel="apple-touch-icon" sizes="144x144" href="<?= base_url('assets/images/favicon/apple-icon-144x144.png') ?>">
    <link rel="apple-touch-icon" sizes="152x152" href="<?= base_url('assets/images/favicon/apple-icon-152x152.png') ?>">
    <link rel="apple-touch-icon" sizes="180x180" href="<?= base_url('assets/images/favicon/apple-icon-180x180.png') ?>">
    <link rel="icon" type="image/png" sizes="192x192"  href="<?= base_url('assets/images/favicon/android-icon-192x192.png') ?>">
    <link rel="icon" type="image/png" sizes="32x32" href="<?= base_url('assets/images/favicon/favicon-32x32.png') ?>">
    <link rel="icon" type="image/png" sizes="96x96" href="<?= base_url('assets/images/favicon/favicon-96x96.png') ?>">
    <link rel="icon" type="image/png" sizes="16x16" href="<?= base_url('assets/images/favicon/favicon-16x16.png') ?>">
    <link rel="manifest" href="<?= base_url('assets/images/favicon/manifest.json') ?>">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="<?= base_url('assets/images/favicon/ms-icon-144x144.png') ?>">
    <meta name="theme-color" content="#ffffff">
    <link rel="shortcut icon" href="<?= base_url('assets/images/favicon//favicon.ico') ?>" type="image/x-icon">
    <link rel="icon" href="<?= base_url('assets/images/favicon/favicon.ico') ?>" type="image/x-icon">
    <!-- Favicon Icon -->
    <!-- Favicon and touch icons  -->
    <!-- <link href="icon/apple-touch-icon-48-precomposed.png" rel="apple-touch-icon-precomposed" sizes="48x48">
    <link href="icon/apple-touch-icon-32-precomposed.png" rel="apple-touch-icon-precomposed">
    <link href="icon/favicon.png" rel="shortcut icon"> -->
    <style>
    /* @media only screen and (max-width: 767px) { 
       #header .header-wrap .flat-wrapper.logo {
       width:200px;height:100px;
    }} */
    @media screen and (max-width:991px) {
        .header .logo a img { padding: 0px !important;height: 40px !important;
width: 240px !important; }
        }
        
    .setheader { margin-top: 5%;padding: 50px 0px 0px; }
    .header .header-wrap .logo {
        margin: 17px 0px 0px;
    }
    </style>
</head>
<body class="header-sticky page-loading">   
<div class="loading-overlay">
</div>
<!-- Boxed -->
<div class="boxed">
    <div id="site-header">
        <div class="flat-top">
            <div class="container">
                <div class="row">
                    <div class="flat-wrapper">
                        <div class="custom-info">
                            <span>Have any questions?</span>
                            <span><i class="fa fa-envelope"></i><a href="mailto:support@mastertraderalerts.com" style="color:lightgrey">support@mastertraderalerts.com</a></span>
                        </div>
                    </div><!-- /.flat-wrapper -->
                    <!-- <?php
                    //if($this->session->userdata('UserName') != ''){       $getnotification = $this->common->post_data('http://35.163.229.242/Master_trader/index.php/api/User/get_alerts',array('auth_token' => $this->session->userdata('AuthToken'),'page_nu'=>'0'));
                    //$alert = $getnotification->alerts_data;
                    ?> -->
                <?php
                if ($this->session->userdata('UserName') != '') { ?>
                    <style type="text/css">
                        .label {
                            background-color:#E66454;
                            display: block;
                            font-size: 8px;
                            line-height: 10px;
                            padding: 1px 4px;
                            position: absolute;
                            right: 7px;
                            top: 0;
                            border-radius: 0.25em;
                            color: #fff;
                            display: inline;
                            font-size: 75%;
                            font-weight: bold;
                            line-height: 1;
                            text-align: center;
                            vertical-align: baseline;
                            white-space: nowrap;
                        }
                        .hover_color:hover{
                            
                            cursor: pointer;
                        }
                    </style>
                    <div class="col-md-3"></div>
        <div class="col-md-3 pull-right col-xs-12">
            <div class="row">
            <div class="col-md-8 col-xs-8"></div>
                <div class="col-md-2 col-xs-2" title="Alert" style="width: 15%;">
                    <ul>
                        <li>
                            <div >
                                <span class="label" id="totalalert">0</span><i class="nav-icon fa fa-bell hover_color" style="font-size:20px;color:#ffffff;padding:5px 10px;" data-toggle="dropdown" ></i>

                                <ul class="dropdown-menu scroll-menu scroll-menu-2x" role="menu" aria-labelledby="menu1" style="width: 275px;margin-left: -208px !important">
                                    <div id="apendalert" class="slimScrollDiv container" style="position: relative; overflow: auto; width: auto; height: 250px;">
                                    </div>
                                </ul>

                            </div>
                        </li>
                    </ul>
                </div>
                <div class="col-md-2 col-xs-2" title="Notification" style="width: 15%;">
                    <ul>
                        <li>
                            <div >
                                <span class="label" id="totalnoti">0</span><i class="nav-icon fa fa-bullhorn dropdown-toggle hover_color" style="font-size:20px;color:#ffffff;padding:5px 12px;" data-toggle="dropdown" ></i>

                                <ul class="dropdown-menu scroll-menu scroll-menu-2x" role="menu" aria-labelledby="menu1" style="width: 300px;margin-left: -246px !important">
                                    <div id="apendnoti" class="slimScrollDiv container" style="position: relative; overflow: auto; width: auto; height: 250px;">
                                    </div>
                                </ul>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
<?php } ?>
    </div><!-- /.row -->
  </div><!-- /.container -->
</div><!-- /.flat-top -->

    <header id="header" class="header clearfix"> 
        <div class="header-wrap clearfix header-style02">
            <div class="container">
                <div class="row">
                    <div class="flat-wrapper">
                        <div id="logo" class="logo col-xs-3">
                            <a href="<?= base_url() ?>">
                                <img src="<?= base_url('assets/images/logo.png') ?>" alt="images">
                            </a>
                        </div><!-- /.logo -->
                        <div class="btn-menu">
                            <span></span>
                        </div><!-- //mobile menu button -->
                        <div class="nav-wrap">        
                            <nav id="mainnav" class="mainnav"> 
                        <?php if ($this->session->userdata('UserName') == '') { ?>
                                <!-- <div class="menu-extra"> -->
                                    <ul>
                                        <li>
                                            <a href="<?= base_url('Login') ?>">
                                                <i class="fa fa-user"></i>&nbsp;Login
                                            </a>  
                                        </li>
                                    </ul>
                                <!-- </div> --> <!-- /.menu-extra-->
                                <?php
                                } else {
                                    $disname = !empty($this->session->userdata('UserName')) ? $this->session->userdata('UserName') : 'Profile';
                                    ?>
                                    <ul>
                                    <li><a class="dropdown-toggle m" data-toggle="dropdown" style="font-weight: normal;"><i class="fa fa-user"></i>&nbsp;<?= $disname ?></a>
                                        <ul class="dropdown-menu pad">
                                            <li><a href="<?= base_url('Profile') ?>"><i class="fa fa-user fa-lg"></i> My Profile</a></li>
                                            <li><a href="<?= base_url('Login/Logout') ?>"><i class="fa fa-sign-out fa-lg"></i> Log out</a></li>
                                        </ul>
                                    </li>
                                </ul>
                            <?php } ?>
                                <ul class="menu">
                                <li <?= 'class="' . $this->common->active_class('Home') . '"' ?>>
                                    <a href="<?= base_url() ?>">Home</a>
                                </li>
                                <li <?= 'class="' . $this->common->active_class('Membership') . '"' ?>><a href="<?= base_url('Membership/member') ?>">Membership</a></li>
                                <li <?= 'class="' . $this->common->active_class('Affiliate') . '"' ?>><a href="<?= base_url('Affiliate') ?>">Affiliate</a></li>
                                <li  <?= 'class="' . $this->common->active_class('Calculate') . '"' ?>> <a href="<?= base_url('Calculate') ?>">Compound Interest</a></li>
                                <li  <?= 'class="' . $this->common->active_class('About') . '"' ?>> <a href="<?= base_url('About/abouts') ?>">About MTA</a></li>
                                <li  <?= 'class="' . $this->common->active_class('Contact') . '"' ?>><a href="<?= base_url('Contact') ?>">Contact US</a></li>
                            </ul><!-- /.menu -->
                        </nav><!-- /.mainnav -->  
                    </div><!-- /.nav-wrap -->
                </div><!-- /.flat-wrapper -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </div><!-- /.header-inner --> 
    </header><!-- /.header -->
</div><!-- /.site-header -->