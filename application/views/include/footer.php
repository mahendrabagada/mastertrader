      <style>
          .abc:hover
          {
            color:black !important;
          }
      </style>
       <div class="footer-content">
                <div class="container">
                    <div class="row">
                        <div class="flat-wrapper">
                            <div class="ft-wrap clearfix">
                                <div class="social-links">
                                    <a href="#"><i class="fa fa-twitter"></i></a>
                                    <a href="#"><i class="fa fa-facebook"></i></a>
                                   
                                    
                                    
                                </div>
                                <div class="copyright">
                                    <div class="copyright-content">
                                        Copyright © 2017 MasterTraderAlerts.com  
                                    </div>
                                </div>
                                <div class="copyright">
                                    <div class="copyright-content">
                                       &nbsp&nbsp<a class="abc" href="<?= base_url('Home/Term_And_Conditions') ?>" style="color:darkorange">Terms and Conditions</a>  
                                    </div>
                                </div>
                                <div class="copyright">
                                    <div class="copyright-content">
                                       &nbsp&nbsp<a class="abc" href="<?= base_url('Home/Disclaimer') ?>" style="color:darkorange">Disclaimer</a>  
                                    </div>
                                </div>
                            </div><!-- /.ft-wrap -->
                        </div><!-- /.flat-wrapper -->
                    </div><!-- /.row -->
                </div><!-- /.container -->
            </div><!-- /.footer-content -->

        <!-- Go Top -->
        <a class="go-top">
            <i class="fa fa-chevron-up"></i>
        </a>   
        <?php if($this->session->userdata('UserName') != '' && $this->session->userdata('UserLoginStatus') == '1'){ 
              if($this->router->fetch_class() != 'Chat'){
          ?> 
            <div class="btmbtn">
                <a href="<?= base_url('Chat') ?>" class="btn" >Chat</a>
            </div>
        <?php } } ?>
    </div>
    
    <!-- Javascript -->
    <script type="text/javascript" src="<?= base_url('assets/javascript/jquery.min.js') ?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/javascript/bootstrap.min.js') ?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/javascript/jquery.easing.js') ?>"></script> 
    <script type="text/javascript" src="<?= base_url('assets/javascript/owl.carousel.js') ?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/javascript/jquery-waypoints.js') ?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/javascript/jquery.flexslider-min.js') ?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/javascript/jquery-countTo.js') ?>"></script> 
    <script type="text/javascript" src="<?= base_url('assets/javascript/jquery.cookie.js') ?>"></script>
   <!--  <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?sensor=false"></script>
   <script type="text/javascript" src="<?= base_url('assets/javascript/gmap3.min.js') ?>"></script> -->
    <script src="<?= base_url('assets/javascript/sweetalert.min.js') ?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/javascript/validation.js') ?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/javascript/jquery-validate.js') ?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/javascript/parallax.js') ?>"></script>
    
    <script type="text/javascript" src="<?= base_url('assets/javascript/main.js') ?>"></script>

    <!-- Revolution Slider -->
    <script type="text/javascript" src="<?= base_url('assets/javascript/jquery.themepunch.tools.min.js') ?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/javascript/jquery.themepunch.revolution.min.js') ?>"></script>
    <script type="text/javascript" src="<?= base_url('assets/javascript/slider.js') ?>"></script>
    <style>
    .btmbtn{ 
    position: fixed;
    bottom: 0px;
    right: 0px;
    z-index: 1000 !important;
    margin-right: 20px !important; }
    .btmbtn a {background-color: rgb(65, 117, 185);
    width: 100%;
    display: block;
    padding: 0px 100px;
    font-size: 22px;
    color: white;
    font-weight: 700;
    border-radius: 5px 5px 0px 0px;}
    .sweet-alert { 
        width:300px !important;
        padding: 10px;
        top: 50%;
        margin-left: -151px; 
    }
    .sweet-alert button {
       width: 74px;
       height: 46px;
       padding: 0px 0px !important;
    }
    </style>
     <?php if( $phpsuccess = $this->session->flashdata('phpsuccess')):  ?>
    <script>swal({title:"<h6 style='color:rgb(56, 102, 30);'><?= $phpsuccess;?></h6>",html:true});</script>
    <?php endif;?> 
    <?php if( $phperror = $this->session->flashdata('phperror')):  ?>
    <script>swal({title:"<h6 style='color:#d9534f;'><?= $phperror;?></h6>",html:true});</script>
    <?php endif;?>
         <?php if($this->session->userdata('UserName') != '' && $this->session->userdata('UserLoginStatus') == '1'){ ?>
    
    <?php } ?>
     <?php if($this->session->userdata('UserName') != '' && $this->session->userdata('UserLoginStatus') == '1'){ ?>
    <script type="text/javascript">
    $(document).ready(function getalert(){ 
        var url = '<?= base_url("Home/getalert") ?>';
        $.ajax({
          type:"post",
          url:url,
          dataType:'JSON',
          success:function(data)
          {
            //$('#apendalert').html(data['return']);
            if(data['total'] == 0){
              $('#apendalert').css({
                position: 'relative',
                overflow: 'auto',
                width: 'auto',
                height: '20px'
              });
              $('#apendalert').html('<span style="font-size: 15px;font-weight: bold;color: blue;">No Alert Found</span>');
            } else {
              $('#apendalert').html(data['return']);
            }
             $('#totalalert').html(data['total']);
             if($("#chatalert").is(':visible')){
                $('#chatalert').html('<ul>'+data['return']+'</ul>');
            }
            setTimeout(getalert, 6000);
          }
          });
    });
     $(document).ready(function getnoti(){ 
        var url = '<?= base_url("Home/getnotification") ?>';
        $.ajax({
          type:"post",
          url:url,
          dataType:'JSON',
          success:function(data)
          {
            console.log(data['total']);
            $('#apendnoti').html(data['return']);
            $('#totalnoti').html(data['total']);
            setTimeout(getnoti, 6000);
          }
          });
    });
    </script>
    <?php } ?>
    <!--End of Zendesk Chat Script--> 
</body>
</html>