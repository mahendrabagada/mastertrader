<?php $this->load->view('include/header'); ?>
        <!-- Slider -->
        <div class="tp-banner-container">
            <div class="tp-banner" >
                <ul>
                    <li data-transition="slidedown" data-slotamount="7" data-masterspeed="1000" data-saveperformance="on">
                        <img src="<?= base_url('assets/images/slides/img.jpg') ?>" alt="slider-image" />
                        <div class="tp-caption sfl title-slide text-center" data-x="center" data-y="203" data-speed="1000" data-start="1000" data-easing="Power3.easeInOut">                            
                            Instant Alerts and Trading Support
                        </div>  
                            
                        <div class="tp-caption sfl flat-button-slider bg-button-slider-333333" data-x="398" data-y="473" data-speed="1000" data-start="2000" data-easing="Power3.easeInOut"><a href="contact.html">Contact Us</a></div>

                        <div class="tp-caption sfr flat-button-slider" data-x="588" data-y="473" data-speed="1000" data-start="2000" data-easing="Power3.easeInOut"><a href="getstarted.html">Get Started</a></div>
                        <div class="tp-caption sfb" data-x="center" data-y="bottom" data-speed="1000" data-start="2000" data-easing="Power3.easeInOut">
                        </div>
                    </li>
                    
                    <li data-transition="random-static" data-slotamount="7" data-masterspeed="1000" data-saveperformance="on">
                        <img src="<?= base_url('assets/images/slides/background2.jpg') ?>" alt="slider-image" />
                        <div class="tp-caption sfl title-slide text-center" data-x="center" data-y="203" data-speed="1000" data-start="1000" data-easing="Power3.easeInOut">                            
                            Unmatched services right<br> at your fingertips
                        </div>  
                            
                        <div class="tp-caption sfl flat-button-slider bg-button-slider-333333" data-x="398" data-y="473" data-speed="1000" data-start="2000" data-easing="Power3.easeInOut"><a href="#">Read More</a></div>

                        <div class="tp-caption sfr flat-button-slider" data-x="588" data-y="473" data-speed="1000" data-start="2000" data-easing="Power3.easeInOut"><a href="#">Contact Us</a></div>
                        <div class="tp-caption sfb" data-x="center" data-y="bottom" data-speed="1000" data-start="2000" data-easing="Power3.easeInOut">
                            
                        </div>
                    </li>

                    <li data-transition="random-static" data-slotamount="7" data-masterspeed="1000" data-saveperformance="on">
                        <img src="<?= base_url('assets/images/slides/background.jpg') ?>" alt="slider-image" />
                        <div class="tp-caption sfl title-slide text-center" data-x="center" data-y="203" data-speed="1000" data-start="1000" data-easing="Power3.easeInOut">                            
                            Always the right information<br> for high valued trading
                        </div>  
                            
                        <div class="tp-caption sfl flat-button-slider bg-button-slider-333333" data-x="398" data-y="473" data-speed="1000" data-start="2000" data-easing="Power3.easeInOut"><a href="#">Read More</a></div>

                        <div class="tp-caption sfr flat-button-slider" data-x="588" data-y="473" data-speed="1000" data-start="2000" data-easing="Power3.easeInOut"><a href="#">Contact Us</a></div>

                        <div class="tp-caption sfb" data-x="center" data-y="bottom" data-speed="1000" data-start="2000" data-easing="Power3.easeInOut">
                        </div>
                    </li>

                    
                </ul>
            </div>
        </div>
        <!-- About text -->
        <div class="flat-row bg-about pad-top0px pad-bottom0px clearfix">
            <div class="about-text-2 item-three-column bg-scheme">
                <h4 class="title">MISSION STATEMENT</h4>
                <div class="content">Equities can only do one of three things… Go up, Down or remain Flat. Our proprietary strategy is formulated to profit from any directional swings. With 22+ years of experience in trading with the same fluid strategy, it is now Mastertraderalerts’ mission to provide you with the alerts to profit by imitating our order sequences at your own will.</div>
                <div class="flat-divider d40px"></div>
               
            </div><!-- /.item-three-column -->
            <div class="about-text-2 item-three-column">
                <h4 class="title">STRATEGY</h4>
                <div class="content">Our positive results are derived from our proprietary trading formula, which encompasses 7 to 9 technical indicators. When 4 or more of our indicators align towards the implied direction, we execute our alerts to reflect a winning trade.</div>
                <div class="flat-divider d40px"></div>
                
            </div><!-- /.item-three-column -->
            <div class="about-text-2 item-three-column bg-scheme">
                <h4 class="title">GOAL</h4>
                <div class="content">We strive to compound 2%+ daily gains. Although, the recent years’ market volatility has blessed us with the opportunity of profiting a multiple of the above mentioned 2% on average. The purpose of our membership is to bring together retail traders to constantly profit from our victorious outcome.</div>
                <div class="flat-divider d40px"></div>
                >
            </div><!-- /.item-three-column -->
        </div><!-- /.flat-row -->
<?php $this->load->view('include/footer'); ?>