<div class="flat-row pad-top90px pad-bottom90px">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="iconbox style_4">
                                    <div class="box-header">
                                        <div class="box-icon"><i class="fa icons icon-diamond"></i></div>
                                        <h5 class="box-title">Experienced</h5>
                                    </div>
                                    <div class="box-content">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer imper diet iaculi sips maliquet ultricies. Sed a tincidunt enim. Maecenas ultrices viverra.                                        
                                    </div>
                                </div><!-- /.iconbox -->
                            </div><!-- /.col-md-4 -->
                            <div class="col-md-4">
                                <div class="iconbox style_4">
                                    <div class="box-header">
                                        <div class="box-icon"><i class="fa icons icon-chart"></i></div>
                                        <h5 class="box-title">Superior Quality</h5>
                                    </div>
                                    <div class="box-content">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer imper diet iaculi sips maliquet ultricies. Sed a tincidunt enim. Maecenas ultrices viverra.                                        
                                    </div>
                                </div><!-- /.iconbox -->
                            </div><!-- /.col-md-4 -->
                            <div class="col-md-4">
                                <div class="iconbox style_4">
                                    <div class="box-header">
                                        <div class="box-icon"><i class="fa icons icon-trophy"></i></div>
                                        <h5 class="box-title">Honest &amp; Dependable</h5>
                                    </div>
                                    <div class="box-content">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer imper diet iaculi sips maliquet ultricies. Sed a tincidunt enim. Maecenas ultrices viverra.                                        
                                    </div>
                                </div><!-- /.iconbox -->
                            </div><!-- /.col-md-4 -->
                        </div><!-- /.row -->

                        <div class="flat-divider d50px"></div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="iconbox style_4">
                                    <div class="box-header">
                                        <div class="box-icon"><i class="fa icons icon-present"></i></div>
                                        <h5 class="box-title">Competitive Rates</h5>
                                    </div>
                                    <div class="box-content">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer imper diet iaculi sips maliquet ultricies. Sed a tincidunt enim. Maecenas ultrices viverra.                                        
                                    </div>
                                </div><!-- /.iconbox -->
                            </div><!-- /.col-md-4 -->
                            <div class="col-md-4">
                                <div class="iconbox style_4">
                                    <div class="box-header">
                                        <div class="box-icon"><i class="fa icons icon-envelope-open"></i></div>
                                        <h5 class="box-title">Free Consultation</h5>
                                    </div>
                                    <div class="box-content">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer imper diet iaculi sips maliquet ultricies. Sed a tincidunt enim. Maecenas ultrices viverra.                                        
                                    </div>
                                </div><!-- /.iconbox -->
                            </div><!-- /.col-md-4 -->
                            <div class="col-md-4">
                                <div class="iconbox style_4">
                                    <div class="box-header">
                                        <div class="box-icon"><i class="fa icons icon-globe"></i></div>
                                        <h5 class="box-title">Professional</h5>
                                    </div>
                                    <div class="box-content">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer imper diet iaculi sips maliquet ultricies. Sed a tincidunt enim. Maecenas ultrices viverra.                                        
                                    </div>
                                </div><!-- /.iconbox -->
                            </div><!-- /.col-md-4 -->
                        </div><!-- /.row -->
                    </div><!-- /.container -->
                </div><!-- /.flat-row -->