<?php $this->load->view('include/header'); ?>
<!-- <link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap.min.css" rel="stylesheet"> -->

<style type="text/css">
.panel-title {display: inline;font-weight: bold;}
.checkbox.pull-right { margin: 0; }
.pl-ziro { padding-left: 0px; }
label {
    display: inline-block;
    font-weight: bold;
    margin-bottom: 5px;
}
a{
	font-size: 15px;
}
</style>
<div class="container" style="margin-top:1%;margin-bottom:10%;">
  <div class="row">
    <div class="col-md-6">
        <div class="list-group ls_grp">
          <a href="#" class="list-group-item active us_hd">
            USER INFORMATION
          </a>
          <a href="#" class="list-group-item list-group-item-action">
            <dl class="dl-horizontal">
                  <dt class="dt_mng">First Name</dt>
                  <dd class="ash cap"><?= $regdata['first_name'] ?></dd>
            </dl>
          </a>
          <a href="#" class="list-group-item list-group-item-action">
              <dl class="dl-horizontal">
                  <dt class="dt_mng">Last Name</dt>
                  <dd class="ash cap"><?= $regdata['last_name'] ?></dd>
            </dl>
          </a>
          <a href="#" class="list-group-item list-group-item-action">
              <dl class="dl-horizontal">
                  <dt class="dt_mng">Username</dt>
                  <dd><?= $regdata['username'] ?></dd>
            </dl>
          </a>
           <a href="#" class="list-group-item list-group-item-action">
              <dl class="dl-horizontal">
                  <dt class="dt_mng">Email Id </dt>
                  <dd class="e_mg"><?= $regdata['email'] ?></dd>
            </dl>
          </a>
        </div>
        <div class="list-group">
          <a href="#" class="list-group-item active us_hd">
            MEMBERSHIP INFORMATION
          </a>
          <div class="list-group-item list-group-item-action">
            <dl class="dl-horizontal">
                <dt class="dt_mng">Plan</dt>
                <dd class="ash "><?php if($this->router->fetch_method() == 'upgrade'){?>
                <div class="field-wrap">
                    <select name="PlanId" id="PlanId" required autocomplete="off">
                      <option value="">Select Your Membership Plan</option>
                      <?php foreach ($plandata as $key => $value) { 
                        foreach ($value as $key1 => $val) { ?>
                      <option data-name="<?= $val->plan_name ?>" data-type="<?= $val->plan_type ?>" data-amount="<?= $val->amount  ?>" value="<?= $val->id;  ?>" <?php if($this->session->userdata('UserPlanId') == $val->id) { echo 'selected="selected"'; } ?>><?= $val->plan_name." ".$val->plan_type." $".$val->amount  ?></option>
                      <?php } } ?>
                    </select>
                  </div>
                <?php
                    } else { echo $plan->plan_name; } ?></dd>
            </dl>
          </div>
          <a href="#" class="list-group-item list-group-item-action">
              <dl class="dl-horizontal">
                  <dt class="dt_mng">Frequency</dt>
                  <dd class="" id="type"><?= $plan->plan_type ?></dd>
            </dl>
          </a>
           <a href="#" class="list-group-item list-group-item-action">
              <dl class="dl-horizontal">
                  <dt class="dt_mng">Amount Paid</dt>
                  <dd class="" id="paid">$<?= $plan->amount ?></dd>
            </dl>
          </a>
          <a href="#" class="list-group-item list-group-item-action">
              <dl class="dl-horizontal">
                  <dt class="dt_mng">Plan Validity</dt>
                  <dd class="" id="expdate"><?= date("d/m/Y", strtotime($this->session->userdata('SubExpDate'))); ?>
                  </dd>
            </dl>
          </a>
        </div>
    </div>
    <div class="col-md-6">
        <form role="form" action="<?= base_url('Login/maketransaction') ?>" method="POST">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            Payment Details
                        </h3>
                    </div>
                    <div class="panel-body">
                        <input type="hidden" name="first_name" value="<?= $regdata['first_name'] ?>"> 
                        <input type="hidden" name="last_name" value="<?= $regdata['last_name'] ?>"> 
                        <input type="hidden" name="email" value="<?= $regdata['email'] ?>">
                        <input type="hidden" name="username" value="<?= $regdata['username'] ?>">
                        <input type="hidden" name="password" value="<?= $regdata['password'] ?>">
                        <input type="hidden" id="plan_id" name="plan_id" value="<?= $regdata['plan_id'] ?>"> 
                        <div class="form-group">
                            <label for="cardNumber">
                                CARD NUMBER</label>
                            <div class="input-group">
                                <input type="text" name="CardNumber" class="form-control" id="cardNumber" placeholder="Valid Card Number"
                                    required autofocus />
                                <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="">Name</label>
                            <div class="input-group">
                                <input type="text" name="CardHolderName" class="form-control" id="CardHolderName" placeholder="Card Holder Name"
                                    required />
                                <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-7 col-md-7">
                                <div class="form-group">
                                    <label for="expityMonth">
                                        EXPIRY DATE</label>
                                        <div class="col-xs-12 col-lg-12" style="padding-left:0px;">
    		                                <div class="col-xs-6 col-lg-6" style="padding-left:0px;">
    		                                    <input type="text" name="ExpiryMonth" maxlength="2" class="form-control" id="expiryMonth" placeholder="MM" required />
    		                                </div>
    		                                <div class="col-xs-6 col-lg-6" style="padding-left:0px;">
    		                                    <input type="text" name="ExpiryYear" maxlength="2" class="form-control" id="expiryYear" placeholder="YY" required /></div>
    		                            </div>
                                </div>
                            </div>
                            <div class="col-xs-5 col-md-5 pull-right">
                                <div class="form-group">
                                    <label for="cvCode">
                                        CVV CODE</label>
                                    <input type="password" name="Cvv" maxlength="3" class="form-control" id="cvCode" placeholder="CVV" required />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <input type="hidden" id="amount" name="amount" value="<?= $amount ?>">
                <ul class="nav nav-pills nav-stacked">
                    <li class="active"><a href="#"><span class="badge pull-right amt">$<?= $amount ?></span> Final Payment</a>
                    </li>
                </ul>
                <br/>
                <input type="submit" class="btn btn-success btn-lg btn-block" role="button">
            </form>
          </div><!-- col-md-6 -->
    </div><!-- row -->
</div>

		
<?php $this->load->view('include/footer'); ?>
<script>
    $(document).ready(function() {
        <?php if($this->session->userdata('UserName') == ''){ ?>
        /*var d = new Date();
            d.setMonth( d.getMonth( ) + 1 );
            var aftone = d.getDate( )  + '/' + ( d.getMonth( ) + 1 ) + '/' + d.getFullYear( );
        $('#expdate').html(aftone);*/
        var fullDate = new Date();
        var twoDigitMonth = ((fullDate.getMonth().length+1) === 1)? (fullDate.getMonth()+1) : '0' + (fullDate.getMonth()+1);
        var currentDate = fullDate.getDate() + "/" + twoDigitMonth + "/" + fullDate.getFullYear();
        $('#expdate').html(currentDate);
        <?php } ?>
        $(this).on('change','#PlanId',function(){
           var d = new Date();
            d.setMonth( d.getMonth( ) + 1 );
            var aftone = d.getDate( )  + '/' + ( d.getMonth( ) + 1 ) + '/' + d.getFullYear( );
            var type = $(this).find(":selected").data('type');
            var amount = $(this).find(":selected").data('amount');
            $('#paid').html('$'+amount);
            $('#type').html(type);
            $('#plan_id').val($(this).val());
            $('#amount').val(amount);
            $('.amt').html('$'+amount);
            $('#expdate').html(aftone);
        });
    });
</script>