<?php $this->load->view('include/header'); error_reporting(0) ?>
<style type="text/css">
    .cen h3 { text-align: center;text-transform: capitalize; }
    .iconbox .marbtm { margin-bottom: 5%; }
</style>
<!-- Slider -->
<div class="tp-banner-container">
    <div class="tp-banner" >
        <ul>
            <li data-transition="slidedown" data-slotamount="7" data-masterspeed="1000" data-saveperformance="on">
                <img src="<?= base_url('assets/images/slides/img.jpg') ?>" alt="slider-image" />
                <div class="tp-caption sfl title-slide text-center" data-x="center" data-y="203" data-speed="1000" data-start="1000" data-easing="Power3.easeInOut">                            
                    Become a better trader <br><br>  without the guesswork
                </div>  

                <div class="tp-caption sfl flat-button-slider bg-button-slider-333333" data-x="398" data-y="473" data-speed="1000" data-start="2000" data-easing="Power3.easeInOut"><a href="<?= base_url('Contact') ?>">Contact Us</a></div>

                <div class="tp-caption sfr flat-button-slider" data-x="588" data-y="473" data-speed="1000" data-start="2000" data-easing="Power3.easeInOut"><a href="<?= base_url('Login') ?>">Get Started</a></div>
                <div class="tp-caption sfb" data-x="center" data-y="bottom" data-speed="1000" data-start="2000" data-easing="Power3.easeInOut">
                </div>
            </li>

            <li data-transition="random-static" data-slotamount="7" data-masterspeed="1000" data-saveperformance="on">
                <img src="<?= base_url('assets/images/slides/background2.jpg') ?>" alt="slider-image" />
                <div class="tp-caption sfl title-slide text-center" data-x="center" data-y="203" data-speed="1000" data-start="1000" data-easing="Power3.easeInOut">                            
                    Discover high-quality low-priced stocks without<br> being stuck in front of your computer all day.
                </div>  

                <div class="tp-caption sfl flat-button-slider bg-button-slider-333333" data-x="398" data-y="473" data-speed="1000" data-start="2000" data-easing="Power3.easeInOut"><a href="#">Read More</a></div>

                <div class="tp-caption sfr flat-button-slider" data-x="588" data-y="473" data-speed="1000" data-start="2000" data-easing="Power3.easeInOut"><a href="<?= base_url('Contact') ?>">Contact Us</a></div>
                <div class="tp-caption sfb" data-x="center" data-y="bottom" data-speed="1000" data-start="2000" data-easing="Power3.easeInOut">

                </div>
            </li>

            <li data-transition="random-static" data-slotamount="7" data-masterspeed="1000" data-saveperformance="on">
                <img src="<?= base_url('assets/images/slides/background.jpg') ?>" alt="slider-image" />
                <div class="tp-caption sfl title-slide text-center" data-x="center" data-y="203" data-speed="1000" data-start="1000" data-easing="Power3.easeInOut">                            
                    Ready to get on the fast track to<br> becoming a millionaire stock trader?
                </div>  

                <div class="tp-caption sfl flat-button-slider bg-button-slider-333333" data-x="398" data-y="473" data-speed="3000" data-start="2000" data-easing="Power3.easeInOut"><a href="<?= base_url('Membership/member') ?>">Join Us!</a></div>

                <div class="tp-caption sfr flat-button-slider" data-x="588" data-y="473" data-speed="1000" data-start="2000" data-easing="Power3.easeInOut"><a href="<?= base_url('Contact') ?>">Contact Us</a></div>

                <div class="tp-caption sfb" data-x="center" data-y="bottom" data-speed="1000" data-start="2000" data-easing="Power3.easeInOut">
                </div>
            </li>
        </ul>
    </div>
</div>

<div class="row">
    <span class="myFontClass"><marquee loop="true" height="60" bgcolor="#3C3C3C" id="mrtext" style="text-align: center;padding-top: 3px;font-size: 31px;"><?php
            $massage = $this->common->post_data('http://35.163.229.242/Master_trader/index.php/api/User/get_website_text', null);
              $data = $massage->website_text;
         echo  $data->website_text;
            ?></marquee></span>
</div>

<div class="flat-row pad-top60px pad-bottom60px">
    <div class="container">
        <div class="row">
            <div class="col-md-3">
                <div class="iconbox style_2">
                    <div class="box-header">
                        <div class="box-icon">
                            <i class="fa fa-comment"></i>
                        </div>
                        <h5 class="box-title">Unlimited Chat</h5>
                    </div>
                    <div class="box-content">
                        <span class="font-size-14px">The professional management of a business doesn’t live without accurate information and timely.</span>                                                                        
                    </div>
                </div>
            </div><!-- /.col-md-3 -->
            <div class="col-md-3">
                <div class="iconbox style_2">
                    <div class="box-header">
                        <div class="box-icon">
                            <i class="fa fa-bell"></i>
                        </div>
                        <h5 class="box-title">Realtime Trade Alerts</h5>
                    </div>
                    <div class="box-content">
                        <span class="font-size-14px">For those looking to invest in the<br>growth of your business or create a new business.</span>                                                                        
                    </div>
                </div>
            </div><!-- /.col-md-3 -->
            <div class="col-md-3">
                <div class="iconbox style_2">
                    <div class="box-header">
                        <div class="box-icon">
                            <i class="fa icons icon-globe"></i>
                        </div>
                        <h5 class="box-title">Result Oriented</h5>
                    </div>
                    <div class="box-content">
                        <span class="font-size-14px">We are result oriented.<br> All of our Alerts sent is gains are shown and verified.</span>
                    </div>
                </div>
            </div><!-- /.col-md-3 -->
            <div class="col-md-3">
                <div class="iconbox style_2">
                    <div class="box-header">
                        <div class="box-icon">
                            <i class="fa fa-desktop"></i>
                        </div>
                        <h5 class="box-title">Cross-Platform</h5>
                    </div>
                    <div class="box-content">
                        <span class="font-size-14px">Whether you're on a Windows, a Mac, or prefer a mobile app instead, we have you covered.</span>                                                                        
                    </div>
                </div>
            </div><!-- /.col-md-3 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.flat-row -->
<!-- End Second Section -->
<!-- About text -->
<div class="flat-row bg-about pad-top0px pad-bottom0px clearfix">
    <div class="about-text-2 item-three-column bg-scheme">
        <h4 class="title">MISSION STATEMENT</h4>
        <div class="content">Equities can only do one of three things… Go up, Down or remain Flat. Our proprietary strategy is formulated to profit from any directional swings. With 22+ years of experience in trading with the same fluid strategy, it is now Mastertraderalerts’ mission to provide you with the alerts to profit by imitating our order sequences at your own will.</div>
        <div class="flat-divider d40px"></div>

    </div><!-- /.item-three-column -->
    <div class="about-text-2 item-three-column">
        <h4 class="title">STRATEGY</h4>
        <div class="content">Our positive results are derived from our proprietary trading formula, which encompasses 7 to 9 technical indicators. When 4 or more of our indicators align towards the implied direction, we execute our alerts to reflect a winning trade.</div>
        <div class="flat-divider d40px"></div>

    </div><!-- /.item-three-column -->
    <div class="about-text-2 item-three-column bg-scheme">
        <h4 class="title">GOAL</h4>
        <div class="content">We strive to compound 2%+ daily gains. Although, the recent years’ market volatility has blessed us with the opportunity of profiting a multiple of the above mentioned 2% on average. The purpose of our membership is to bring together retail traders to constantly profit from our victorious outcome.</div>
        <div class="flat-divider d40px"></div>
        >
    </div><!-- /.item-three-column -->
</div><!-- /.flat-row -->
<!-- Third last section -->
<!-- Video -->
<div class="flat-row bg-f7f7f7 pad-top60px">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="flat-video-fancybox text-center">
                    <a class="fancybox" data-type="iframe" href="https://www.youtube.com/embed/p_E9S2nRrwM?autoplay=1"> 
                        <img src="<?= base_url('assets/images/img-single/bg-video2.jpg') ?>" alt="images">
                    </a>
                </div>
            </div><!-- /.col-md-6 -->
            <div class="col-md-6">
                <div class="section-iconlist2">
                    <div class="title-section style_1">
                        <h3 class="title">Why Choose <strong>Us?</strong></h3>  
                        <div class="desc">
                            With over 20 years of experience, we recognize the importance of internationalization for company growth. Approximately 25% of our clients<br>have an international presence.
                        </div>                      
                    </div>
                    <div class="flat-divider d30px"></div>
                    <div class="flat-iconlist">
                        <div class="row">
                            <div class="col-md-6">
                                <ul class="iconlist">
                                    <li><i class="fa fa-cog"></i> Proven Track Record of Success.</li>
                                    <li><i class="fa fa-cog"></i> Actionable Trade Alerts.</li>
                                    <li><i class="fa fa-cog"></i> Mobile Alerts.</li>
                                </ul>
                            </div><!-- /.col-md-6 -->
                            <div class="col-md-6">
                                <ul class="iconlist">
                                    <li><i class="fa fa-cog"></i> Member Chat Rooms.</li>
                                    <li><i class="fa fa-cog"></i> Affiliate Program.</li>
                                    <li><i class="fa fa-cog"></i>  No guessing.</li>
                                </ul>
                            </div><!-- /.col-md-6 --> 
                        </div><!-- /.row -->
                    </div><!-- /.flat-list -->
                    <div class="flat-divider d30px"></div>
                    <div class="group-btn">
                        <a class="button lg dark" href="<?= base_url('Contact') ?>">How can we help you?</a>
                    </div>
                </div><!-- /.section-iconlist2 -->
            </div><!-- /.col-md-6 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.flat-row -->
<!-- End Third last section -->
<div class="flat-row bg-222222">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <div class="title-section style_1">
                    <h3 class="title color-ffffff">Go mobile</h3>
                    <div class="desc" style="color:lightgrey">
                        Get trade alerts right away anywhere and at anytime on your smartphones. Get our app today!
                    </div>
                </div>
            </div><!-- /.col-md-8 -->
            <div class="col-md-4">
                <div class="group-button text-center">
                    <a href="https://play.google.com/storem"><img border="0" alt="GooglePlay" src="<?= base_url('assets/images/GooglePlay.png') ?>" width="180" height="61"></a>
                    <a href="https://itunes.apple.com/in/genre/ios/id36?mt=8"><img border="0" alt="GooglePlay" src="<?= base_url('assets/images/Appstore.png') ?>" width="180" height="61"></a>
                </div>
            </div><!-- /.col-md-4 -->
        </div><!-- /.row -->
    </div><!-- /.container -->
</div><!-- /.flat-row -->
<!-- Testimonial -->

<?php $this->load->view('include/footer'); ?>
<script>
$(document).ready(function newRecord(){ 
    var url = '<?= base_url("Home/martext") ?>';
    $.ajax({
      type:"post",
      url:url,
      success:function(data)
      {
        console.log(data);
        $('#mrtext').html(data);
        setTimeout(newRecord, 60000);
      }
      });
    });

</script>

