<?php $this->load->view('include/header'); ?>

    <div id="site-content">                    
            <section class="flat-row flat-video video-bg bg-playvideo">
                <div class="overlay"></div>        
                <div class="video">
                    <div id="bg-video">
                        <div class="video-section" data-property="{videoURL:'https://www.youtube.com/watch?v=hfDIL3Yy6ug',containment:'.flat-video.video-bg', autoPlay:true, mute:true, startAt:8, opacity:1, vol: 0, realfullscreen:true, quality: 'hd1080', startAt: 12}"></div>
                    </div>
                    <h2 class="title">Welcome to MasterTraderAlerts</h2>
                </div>
            </section>               
           

            <div id="page-body">
                <div class="flat-row pad-top90px pad-bottom90px">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6">
                                <h3>Hello we are Master Traders <br> Established in Los Angeles.</h3>
                            </div><!-- /.col-md-6 -->
                            <div class="col-md-6">
                                <h4>About Us</h4>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer imperdiet iaculis ipsum aliquet ultricies. Sed a tincidunt enim. Maecenas ultrices viverra ligula, vel lobortis ante pulvinar sed deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum.</p>
                            </div><!-- /.col-md-6 -->
                        </div><!-- /.row -->
                    </div><!-- /.container -->
                </div><!-- /.flat-row -->

                <div class="flat-row bg-scheme pad-bottom60px">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-8 col-md-offset-2">
                                <h4 class="about-text-1 text-center">” We recognize the importance of trading at the International levels making your growth on-time and on regularity with our accurate analysis and instant alerts. ”</h4>
                            </div><!-- /.col-md-12 -->
                        </div><!-- /.row -->
                    </div><!-- /.container -->
                </div><!-- /.flat-row -->

                <div class="flat-row pad-top90px pad-bottom90px">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="iconbox style_4">
                                    <div class="box-header">
                                        <div class="box-icon"><i class="fa icons icon-diamond"></i></div>
                                        <h5 class="box-title">Experienced</h5>
                                    </div>
                                    <div class="box-content">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer imper diet iaculi sips maliquet ultricies. Sed a tincidunt enim. Maecenas ultrices viverra.                                        
                                    </div>
                                </div><!-- /.iconbox -->
                            </div><!-- /.col-md-4 -->
                            <div class="col-md-4">
                                <div class="iconbox style_4">
                                    <div class="box-header">
                                        <div class="box-icon"><i class="fa icons icon-chart"></i></div>
                                        <h5 class="box-title">Superior Quality</h5>
                                    </div>
                                    <div class="box-content">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer imper diet iaculi sips maliquet ultricies. Sed a tincidunt enim. Maecenas ultrices viverra.                                        
                                    </div>
                                </div><!-- /.iconbox -->
                            </div><!-- /.col-md-4 -->
                            <div class="col-md-4">
                                <div class="iconbox style_4">
                                    <div class="box-header">
                                        <div class="box-icon"><i class="fa icons icon-trophy"></i></div>
                                        <h5 class="box-title">Honest &amp; Dependable</h5>
                                    </div>
                                    <div class="box-content">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer imper diet iaculi sips maliquet ultricies. Sed a tincidunt enim. Maecenas ultrices viverra.                                        
                                    </div>
                                </div><!-- /.iconbox -->
                            </div><!-- /.col-md-4 -->
                        </div><!-- /.row -->

                        <div class="flat-divider d50px"></div>

                        <div class="row">
                            <div class="col-md-4">
                                <div class="iconbox style_4">
                                    <div class="box-header">
                                        <div class="box-icon"><i class="fa icons icon-present"></i></div>
                                        <h5 class="box-title">Competitive Rates</h5>
                                    </div>
                                    <div class="box-content">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer imper diet iaculi sips maliquet ultricies. Sed a tincidunt enim. Maecenas ultrices viverra.                                        
                                    </div>
                                </div><!-- /.iconbox -->
                            </div><!-- /.col-md-4 -->
                            <div class="col-md-4">
                                <div class="iconbox style_4">
                                    <div class="box-header">
                                        <div class="box-icon"><i class="fa icons icon-envelope-open"></i></div>
                                        <h5 class="box-title">Free Consultation</h5>
                                    </div>
                                    <div class="box-content">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer imper diet iaculi sips maliquet ultricies. Sed a tincidunt enim. Maecenas ultrices viverra.                                        
                                    </div>
                                </div><!-- /.iconbox -->
                            </div><!-- /.col-md-4 -->
                            <div class="col-md-4">
                                <div class="iconbox style_4">
                                    <div class="box-header">
                                        <div class="box-icon"><i class="fa icons icon-globe"></i></div>
                                        <h5 class="box-title">Professional</h5>
                                    </div>
                                    <div class="box-content">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer imper diet iaculi sips maliquet ultricies. Sed a tincidunt enim. Maecenas ultrices viverra.                                        
                                    </div>
                                </div><!-- /.iconbox -->
                            </div><!-- /.col-md-4 -->
                        </div><!-- /.row -->
                    </div><!-- /.container -->
                </div><!-- /.flat-row -->

                <div class="flat-row bg-about pad-top0px pad-bottom0px clearfix">
                    <div class="about-text-2 item-three-column bg-scheme">
                        <h4 class="title">MISSION STATEMENT</h4>
                        <div class="content">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce cursus leo orci, id pharetra odio varius ac. Integer accumsan posuere erat ac porttitor. Vestibulum porttitor est mi, sed pharetra nisi dapibus sed. Maecenas iaculis leo sit amet tempor maximus.</div>
                    </div><!-- /.item-three-column -->
                    <div class="about-text-2 item-three-column">
                        <h4 class="title">VISION STATEMENT</h4>
                        <div class="content">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce cursus leo orci, id pharetra odio varius ac. Integer accumsan posuere erat ac porttitor. Vestibulum porttitor est mi, sed pharetra nisi dapibus sed. Maecenas iaculis leo sit amet tempor maximus.</div>
                    </div><!-- /.item-three-column -->
                    <div class="about-text-2 item-three-column bg-222222">
                        <h4 class="title">VALUES STATEMENT</h4>
                        <div class="content">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce cursus leo orci, id pharetra odio varius ac. Integer accumsan posuere erat ac porttitor. Vestibulum porttitor est mi, sed pharetra nisi dapibus sed. Maecenas iaculis leo sit amet tempor maximus.</div>
                    </div><!-- /.item-three-column -->
                </div><!-- /.flat-row -->

                

                <div class="flat-row parallax parallax2 pad-top90px pad-bottom110px">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="title-section">
                                    <h3 class="title">Our Partners</h3>
                                    <div class="desc color-333333">
                                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Fusce cursus leo orci,<br>id pharetra odio varius ac.
                                    </div>
                                </div>
                            </div><!-- /.col-md-12 -->
                        </div><!-- /.row -->

                        <div class="flat-divider d40px"></div>

                        <div class="row">
                            <div class="col-md-12">
                                <div class="flat-clients style_1" data-item="6" data-nav="false" data-dots="false" data-auto="true">
                                    <div class="client-featured">
                                        <img src="<?= base_url('assets/images/client/1.png') ?>" alt="images">
                                    </div>
                                    <div class="client-featured">
                                        <img src="<?= base_url('assets/images/client/2.png') ?>" alt="images">
                                    </div>
                                    <div class="client-featured">
                                        <img src="<?= base_url('assets/images/client/3.png') ?>" alt="images">
                                    </div>
                                    <div class="client-featured">
                                        <img src="<?= base_url('assets/images/client/4.png') ?>" alt="images">
                                    </div>
                                    <div class="client-featured">
                                        <img src="<?= base_url('assets/images/client/5.png') ?>" alt="images">
                                    </div>
                                    <div class="client-featured">
                                        <img src="<?= base_url('assets/images/client/6.png') ?>" alt="images">
                                    </div>
                                    <div class="client-featured">
                                        <img src="<?= base_url('assets/images/client/1.png') ?>" alt="images">
                                    </div>
                                    <div class="client-featured">
                                        <img src="<?= base_url('assets/images/client/2.png') ?>" alt="images">
                                    </div>
                                    <div class="client-featured">
                                        <img src="<?= base_url('assets/images/client/3.png') ?>" alt="images">
                                    </div>
                                    <div class="client-featured">
                                        <img src="<?= base_url('assets/images/client/4.png') ?>" alt="images">
                                    </div>
                                </div><!-- /.flat-clients -->
                            </div><!-- /.col-md-12 -->
                        </div><!-- /.row -->
                    </div><!-- /.container -->
                </div><!-- /.flat-row -->
            </div><!-- /.page-body -->
        </div><!-- /#site-content -->

<?php $this->load->view('include/footer'); ?>
<script type="text/javascript" src="<?= base_url('assets/javascript/jquery.mb.YTPlayer.js') ?>"></script>
 <script type="text/javascript" src="<?= base_url('assets/javascript/jquery.magnific-popup.min.js') ?>"></script> 
