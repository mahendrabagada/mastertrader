<?php $this->load->view('include/header'); ?>
<div id="site-content">
    <div id="page-header">
        <div class="container">
            <div class="row">
                <div class="page-title">
                    <h2 class="title">Profile</h2>
                </div>                        
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /#page-header -->
<!-- Profile Picture -->
<?php $this->load->view('include/message'); ?>
<div class="container">
  <div class="row">
    <div class="col-lg-3 col-md-2 col-sm-12"><!-- -->
      <img id="profile_image" src="<?= $this->session->userdata('ProfilePicture') ?>" alt="No Image" class="img-polaroid img-thumbnail" style="max-width: 100%;max-height: 140px;">
    </div>
      <div class="col-lg-9 col-md-10 col-sm-12">
        <div class="row-fluid">
        <div class="col-md-12 col-lg-12 col-sm-12"><h5 id="name"><?= $this->session->userdata('FirstName')." ".$this->session->userdata('LastName') ?></h5></div>
        <div class="col-md-12 col-lg-12 col-sm-12"><h5 id="email"><?= $this->session->userdata('Email') ?></h5></div>
      </div>
      <div class="row-fluid">
       <div class="col-md-12 col-lg-12 col-sm-12">
        <table class="table table-responsive table-striped">
          <tr>
            <td>Created On</td>
            <td><?php $date = explode(' ',$this->session->userdata('CreatedOn'));
              echo date("d F Y", strtotime($date[0]))." ".$date[1]; ?></td>
            <td>Subscription Expire</td>
            <td><?php 
              echo date("d F Y", strtotime($this->session->userdata('SubExpDate'))); ?></td>
          </tr>
          <tr>
            <td>Updated On</td>
            <td></td>
            <td>Authentication Tokan</td>
            <td><?= $this->session->userdata('AuthToken') ?></td>
          </tr>
        </table>
        </div>
        <div style="clear:both"></div>
      </div>
    </div>
  </div><!-- row-->
<!-- Profile Picture -->
<div class="row">
    <div class="col-lg-5">
      <form method="post" name="change" action="<?= base_url('Profile/change_password'); ?>">
            <div class="form-group">
            <label>Old Password</label>
              <input class="form-control" name="Old_Password" id="Old_Password" type="password" placeholder="Old Password"/>
            </div><br>
            <label>New Password</label>
            <div class="form-group">
              <input class="form-control" name="Password" id="Password" type="password" placeholder="New Password"/>
            </div><br>
            <label>Retype Password</label>
            <div class="form-group">
              <input class="form-control" name="Re_Password" id="Re_Password" type="password" placeholder="Retype New Password"/>
            </div><br>
            <div class="form-group">      
              <input type="submit" id="change_pwd" name="change_pwd" class="btn btn-success check" value="Change Password" />
              <a href="<?= base_url('Profile'); ?>" style="width:140px;background-color: #fff;border-radius:0px;font-weight:bold;color:#0071BD" name="clear" class="btn btn-primary pull-right btn-lg">Cancel</a>
              <br>
          </div>
        </form>
    </div><!-- col-md-6-->
    <div class="col-lg-5">
      <form enctype="multipart/form-data" action="<?= base_url('Profile/profilepicture');?>" method="post">
            <div class="form-group">
                <label>Choose Files</label>
                <input type="file" id="filename" class="form-control" name="filename"/>
            </div>
            <div class="form-group">
                <input class="form-control" type="submit" id="fileSubmit" name="fileSubmit" value="Change Picture"/>
            </div>
      </form>
    </div><!-- col-md-6-->
  </div><!-- row-->
</div><!-- container-->
  </div>
<?php $this->load->view('include/footer'); ?>
<script>
$(document).ready(function() {
    $(".check").click(function () {
    if(isempty('Old_Password','Old Password') || isempty('Password','Password') || isempty('Re_Password','Retype Password')){
         return false;
    }
    else if($("#Password").val() != $("#Re_Password").val())
    {
      alert('Password and Retype Password Not Matched');
      onfocus('Re_Password');
      return false;
    }
  });
});
</script> 