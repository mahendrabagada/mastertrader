<?php $this->load->view('include/header'); ?>
<style type="text/css" media="screen">
@media only screen and (max-width: 1004px) {
    .bg-playvideo {
        margin-top: 0px !important;
    }
}
@media only screen and (min-width: 1338px) {
    .about-text-2 { padding-bottom: 117px; }
}
 h2 {
    margin: 113px 0px 20px; }
.about-text-2 { padding: 60px 52px 90px; }
</style>
    <div id="site-content" >                    
<!--            <section class="flat-row flat-video">
                <div class="overlay"></div>        
                <div class="video">
                    <div id="bg-video">
                            <div class="video-section" data-property="{videoURL:'http://shipmentscanner.com/MasterTrader/Video.mov',containment:'.flat-video.video-bg', autoPlay:true, mute:true, startAt:8, opacity:1, vol: 0, realfullscreen:true, quality: 'hd1080', startAt: 12}"></div>
                    </div>
                    <h2 class="title" style="text-align: center;">Welcome to MasterTraderAlerts</h2>
                    <h4 class="about-text-1 text-center">&nbsp;&nbsp;"We recognize the importance of trading at the International levels making your growth on-time and on regularity with our accurate analysis and instant alerts."&nbsp;&nbsp;</h4>
                </div>
            </section>-->
         <section class="flat-row flat-video video-bg bg-playvideo">
                <div class="overlay"></div>        
                <div class="video">
                    <div id="bg-video">
                        <div class="video-section" data-property="{videoURL:'https://youtu.be/Thd--viGNjk?list=PL_UUBmTF7Q6Y7fAIaGxBwaoNi6Nrf20Mf',containment:'.flat-video.video-bg', autoPlay:true, mute:true, startAt:8, opacity:1, vol: 0, realfullscreen:true, quality: 'hd1080', startAt: 12}"></div>
                    </div>
                    <h2 class="title">Welcome to MasterTraderAlerts</h2>
                    <h4 class="about-text-1 text-center">&nbsp;&nbsp;"We recognize the importance of trading at the International levels making your growth on-time and on regularity with our accurate analysis and instant alerts."&nbsp;&nbsp;</h4>
                </div>
            </section>  
            <div id="page-body">
                <div class="flat-row bg-f7f7f7 pad-top0px pad-bottom0px">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="section-testimonial">
                            <div class="title-section style_2">
                                <h2 class="title">We love<br>to hear what they say</h2>
                            </div>
                            <div class="flat-divider d20px"></div>
                            <div class="flat-testimonial">
                                <div class="flexslider">
                                    <ul class="slides">
                                        <li>
                                            <div class="testimonial bg-f7f7f7">
                                                <div class="testimonial-content">
                                                    <blockquote>For the past year I have been day trading and remained unsuccessful. After joining and following MasterTraderAlerts I have now compounded my account 3 folds for the last 4 months. MTA gives me the confidence to wake up every morning knowing I have a partner on my side to help me succeed.</blockquote>
                                                </div>
                                                <div class="testimonial-meta">
                                                    <div class="testimonial-author">
                                                        <strong class="author-name">Phil Jordan</strong>
                                                        <div class="author-info"><a href="#" class="company">Allentown, Pennsylvania</a></div>
                                                    </div>
                                                </div>
                                            </div><!-- /.testimonial -->
                                        </li>
                                        <li>
                                            <div class="testimonial bg-f7f7f7">
                                                <div class="testimonial">
                                                    <div class="testimonial-content">
                                                        <blockquote>My dream is to become “MasterTrader. This is my launch pad! How the heck you guys are so on the spot on your every trade? Lol</blockquote>
                                                    </div>
                                                    <div class="testimonial-meta">
                                                        <div class="testimonial-author">
                                                            <strong class="author-name">Farhad Mansouri</strong>
                                                            <div class="author-info"><a href="#" class="company">Irvine, California</a></div>
                                                        </div>
                                                    </div>
                                                </div><!-- /.testimonial -->
                                            </div><!-- /.testimonial -->
                                        </li> 
                                        <li>
                                            <div class="testimonial bg-f7f7f7">
                                                <div class="testimonial">
                                                    <div class="testimonial-content">
                                                        <blockquote>At age 52, it is hard to learn a new skill of technical analysis to become a better trader. With my lack of knowledge, I kept losing my money. With MasterTraderAlerts ( MTA) , I have nothing to worry about. I just follow the same orders, and come out profitable time after time. Thanks MTA!</blockquote>
                                                    </div>
                                                    <div class="testimonial-meta">
                                                        <div class="testimonial-author">
                                                            <strong class="author-name">Audrey Hamilton</strong>
                                                            <div class="author-info"><a href="#" class="company">Atlanta, Georgia</a></div>
                                                        </div>
                                                    </div>
                                                </div><!-- /.testimonial -->
                                            </div><!-- /.testimonial -->
                                        </li>
                                         <li>
                                            <div class="testimonial bg-f7f7f7">
                                                <div class="testimonial">
                                                    <div class="testimonial-content">
                                                        <blockquote>Oh boy, I’m speechless. One word only: Trader behind MasterTraderAlerts is God’s Given. Keep it up folks…</blockquote>
                                                    </div>
                                                    <div class="testimonial-meta">
                                                        <div class="testimonial-author">
                                                            <strong class="author-name">Michelle Bernitsky</strong>
                                                            <div class="author-info"><a href="#" class="company">Las Vegas, NV</a></div>
                                                        </div>
                                                    </div>
                                                </div><!-- /.testimonial -->
                                            </div><!-- /.testimonial -->
                                        </li>         
                                    </ul>
                                </div><!-- /.flexslider -->                                        
                            </div><!-- /.flat-testimonial -->
                        </div><!-- /.section-testimonial -->
                    </div><!-- /.col-md-6 -->
                    <div class="col-md-6">
                        <div class="image-single">
                            <img src="<?= base_url('assets/images/img-single/img-clients.jpg') ?>" alt="images">
                        </div>
                    </div><!-- /.col-md-6 -->
                </div><!-- /.row -->
            </div><!-- /.container -->
        </div><!-- /.flat-row -->

        

                <div class="flat-row bg-about pad-top0px pad-bottom0px clearfix">
                    <div class="about-text-2 item-three-column bg-scheme">
                        <h4 class="title">MISSION STATEMENT</h4>
                        <div class="content">Equities can only do one of three things… Go up, Down or remain Flat. Our proprietary strategy is formulated to profit from any directional swings. With 22+ years of experience in trading with the same fluid strategy, it is now Mastertraderalerts’ mission to provide you with the alerts to profit by imitating our order sequences at your own will.</div>
                    </div><!-- /.item-three-column -->
                    <div class="about-text-2 item-three-column">
                        <h4 class="title">VISION STATEMENT</h4>
                        <div class="content">Our positive results are derived from our proprietary trading formula, which encompasses 7 to 9 technical indicators. When 4 or more of our indicators align towards the implied direction, we execute our alerts to reflect a winning trade.</div>
                    </div><!-- /.item-three-column -->
                    <div class="about-text-2 item-three-column bg-scheme">
                        <h4 class="title">VALUES STATEMENT</h4>
                        <div class="content" style="padding-bottom:27px;">We strive to compound 2%+ daily gains. Although, the recent years’ market volatility has blessed us with the opportunity of profiting a multiple of the above mentioned 2% on average. The purpose of our membership is to bring together retail traders to constantly profit from our victorious outcome.</div>
                    </div><!-- /.item-three-column -->
                </div><!-- /.flat-row -->

                

                
            </div>
        </div>
<?php $this->load->view('include/footer'); ?>
<script type="text/javascript" src="<?= base_url('assets/javascript/jquery.mb.YTPlayer.js') ?>"></script>
 <script type="text/javascript" src="<?= base_url('assets/javascript/jquery.magnific-popup.min.js') ?>"></script> 
