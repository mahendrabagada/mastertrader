<?php $this->load->view('include/header'); ?>
	
		 <!-- Site content -->
        <div id="site-content">
                <div class="container">
                    <div class="row">
                        <div class="page-title">
                            <h2 class="title text-center" style="padding: 10px;">Contact Us</h2>
                        </div>                        
                    </div><!-- /.row -->
                </div><!-- /.container -->
			<div class="row">
			    <div class="col-md-2"></div>
			       <div class="col-md-8">
			       <?php if( $phpsuccess = $this->session->flashdata('phpsuccess')):  ?>
			           <div class="alert alert-success"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><?php echo $phpsuccess;?></div>
			       <?php endif;?> 
			       <?php if( $phperror = $this->session->flashdata('phperror')):  ?>
			           <div class="alert alert-danger"><a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><?php echo $phperror;?></div>
			       <?php endif;?> </div>
			</div>
            <div id="page-body">
                <div class="flat-row pad-top10px pad-bottom20px">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                
                            </div><!-- /.col-md-12 -->
                        </div><!-- /.row -->
                        <div class="flat-divider d40px"></div>
                        <div class="row">
                            <div class="col-md-8 col-md-offset-2">
                                <form  id="contactform1" class="flat-contact-form" method="POST" action="<?= base_url('Contact/Submit') ?>">
                                    <div class="quick-appoinment">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <input type="text" id="name" name="name" class="input-text-name" placeholder="Name" required="required">
                                            </div><!-- /.col-md-6 -->
                                            <div class="col-md-6">
                                                <input type="email" id="email" name="email" class="input-text-email" placeholder="Email" required="required">
                                            </div><!-- /.col-md-6 -->
                                        </div><!-- /.row -->

                                        <div class="flat-divider d30px"></div>

                                        <div class="row">
                                            <!-- <div class="col-md-6">
                                                <input type="text" id="phone" name="phone" class="input-text-phone" placeholder="Phone" required="required">
                                            </div> --><!-- /.col-md-6 -->
                                            <div class="col-md-6">
                                                <input type="text" id="subject" name="subject"  class="input-text-subject" placeholder="Subject" required="required">
                                            </div><!-- /.col-md-6 -->
                                        </div><!-- /.row -->

                                        <div class="flat-divider d30px"></div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <textarea class="textarea-question" id="message" name="message" placeholder="Message" required="required"></textarea>
                                            </div><!-- /.col-md-12 -->
                                        </div><!-- /.row -->

                                        <div class="flat-divider d30px"></div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <input type="submit" id="submit1" value="Send Message" class="input-submit">
                                            </div><!-- /.col-md-12 -->
                                        </div><!-- /.row -->
                                    </div>
                                </form>
                            </div><!-- /.col-md-8 -->
                        </div><!-- /.row -->
                    </div><!-- /.container -->
                </div><!-- /.flat-row -->
            </div><!-- /.page-body -->
        </div><!-- /#site-content -->
	
<?php $this->load->view('include/footer'); ?>
<script> 
                $(document).ready(function(){
                    
                    // only_number_press('PhoneNumber');
                    // only_number_press('MobileNumber');
                    // only_number_press('CurrentPostCode');
                    $('#submit1').click(function(e) {
                     if(isempty('name','Name') || isempty('email','Email') || isempty('subject','Subject') || isempty('message','Message') ){
                      return false;
                     }
                  });

                });
    </script>
