<?php $this->load->view('include/header'); ?>
<link rel="stylesheet" href="http://35.163.229.242/Master_trader/dist/js/quickblox/libs/stickerpipe/css/stickerpipe.min.css">
<link rel="stylesheet" href="<?= base_url('assets/javascript/chat/chat/css/style.css') ?>">
<style>
    .custom-div { height: 433px; max-height: 433px; width:250px; max-width:270px; background-color: #fff; padding-left: 5px; }
    .custom-div .first {padding-left: 20px; font-size: 15px;}
    .custom-div .first .second{ margin-top: 10px; }
    .custom-div .first .second b { color: orange;text-transform: uppercase; }
    .custom-div .first .second b span { color:green; }
    .custom-div .first .third p {word-wrap:break-word; width: 200px;}
    .custom-div .first .fourth { display: block;height: 1px; border: 0; border-top: 1px dotted #ccc;margin: 1em 0; padding: 0; }
    h4 {
        font-size: 16px;
    }
</style>
<div id="site-content">
    <!--     <div id="page-header">
                               <div class="container">
                                   <div class="row">
                                       <div class="page-title">
                                           <h2 class="title">Chat</h2>
                                       </div>  -->                       
    <!--  </div> --><!-- /.row -->
    <!-- </div> --><!-- /.container -->

    <!-- </div> --><!-- /#page-header -->
    <div id="page-body">
        <!-- Main block -->
        <div class="container" style="margin-top:0%">
            <div id="main_block">
                <div class="row">
                    <div class="col-md-3">
                        <div class="panel panel-primary">
                            <div class="panel-body">
                                <h4 class="text-center">Alerts</h4>
                                <div class="pull-right list-group pre-scrollable nice-scroll col-sm-3 custom-div">
                                    <div class="col-md-12 first" align="left" id="chatalert">
                                        <div class="second"> 
                                            <b>$asd</b> <b><span>BUY</span></b> <b>$100 </b>
                                        </div>
                                        <div class="third"> 
                                            <p>sdas</p>
                                            <p>January 10, 2017 at 06:41 AM</p> 
                                        </div>
                                        <div class="fourth">
                                        </div>
                                    </div>
                                </div><!-- pull-right-->
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="panel panel-primary">
                            <div class="panel-body">
                                <div class="row">

                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-4">


                                                <div class="list-header">
                                                    <h4 class="list-header-title">Chat</h4>
                                                </div>
                                                <div class="list-group pre-scrollable nice-scroll" id="dialogs-list">

                                                    <!-- list of chat dialogs will be here -->

                                                </div>
                                            </div>
                                            <div id="mcs_container" class="col-md-8">
                                                <div class="container del-style">
                                                    <div class="content list-group pre-scrollable nice-scroll" id="messages-list">
                                                        <!-- list of chat messages will be here -->
                                                    </div>
                                                </div>

                                                <div><img src="<?= base_url('assets/javascript/chat/chat/images/ajax-loader.gif') ?>" class="load-msg"></div>
                                                <form class="form-inline" role="form" method="POST" action="" onsubmit="return submit_handler(this)">
                                                    <div class="input-group">
                                                        <span class="input-group-btn input-group-btn_change_load">
                                                            <input id="load-img" type="file">
                                                            <button type="button" id="attach_btn" class="btn btn-default" onclick="$('#load-img').click();">
                                                                <i class="icon-photo"></i>
                                                            </button>
                                                        </span>
                                                        <span class="input-group-btn input-group-btn_change_load">
                                                            <button type="button" id="stickers_btn" class="btn btn-default" onclick="">
                                                                <i class="icon-sticker"></i>
                                                            </button>
                                                        </span>

                                                        <span class="input-group-btn" style="width: 100%;">
                                                            <input type="text" class="form-control" id="message_text" placeholder="Enter message">
                                                        </span>

                                                        <span class="input-group-btn">
                                                            <button  type="submit" id="send_btn" class="btn btn-default" onclick="clickSendMessage()">Send</button>
                                                        </span>
                                                    </div>
                                                    <img src="<?= base_url('assets/javascript/chat/chat/images/ajax-loader.gif') ?>" id="progress">
                                                </form>
                                            </div>
                                        </div>
                                    </div><!-- end col-md-6 -->

                                </div><!-- main row of half col-md-6 -->
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div><!-- /#page-body -->
</div><!-- Site content -->
<?php $this->load->view('include/footer'); ?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/quickblox/2.4.0/quickblox.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.nicescroll/3.6.0/jquery.nicescroll.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-timeago/1.4.1/jquery.timeago.min.js"></script>
<script src="<?= base_url('assets/javascript/chat/chat/libs/stickerpipe/js/stickerpipe.js') ?>"></script>
<script src="<?= base_url('assets/javascript/chat/chat/js/config.js') ?>"></script>
<script src="<?= base_url('assets/javascript/chat/chat/js/connection.js') ?>"></script>
<script src="<?= base_url('assets/javascript/chat/chat/js/messages.js') ?>"></script>
<script src="<?= base_url('assets/javascript/chat/chat/js/stickerpipe.js') ?>"></script>
<script src="<?= base_url('assets/javascript/chat/chat/js/ui_helpers.js') ?>"></script>
<script src="<?= base_url('assets/javascript/chat/chat/js/dialogs.js') ?>"></script>
<script src="<?= base_url('assets/javascript/chat/chat/js/users.js') ?>"></script>
<script>
                                                                var user = {
                                                                    id: '<?= $quserid ?>',
                                                                    email: '<?= $qusermail ?>',
                                                                    login: '<?= $quserlogin ?>',
                                                                    pass: '<?= $quserpass ?>'
                                                                };
                                                                currentUser = user;
                                                                /*connectToChat(user);*/
                                                                QB.createSession({login: user.login, password: user.pass}, function (err, res) {
                                                                    if (res) {
                                                                        token = res.token;
                                                                        user.id = res.user_id;

                                                                        mergeUsers([{user: user}]);
                                                                        QB.chat.connect({userId: user.id, password: user.pass}, function (err, roster) {
                                                                            if (err) {
                                                                                console.log(err);
                                                                            } else {
                                                                                //console.log(roster);
                                                                                setupStickerPipe();

                                                                                retrieveChatDialogs();

                                                                                // setup message listeners
                                                                                setupAllListeners();

                                                                                // setup scroll events handler
                                                                                setupMsgScrollHandler();

                                                                                setupStreamManagementListeners();
                                                                            }
                                                                        });
                                                                    } else {
                                                                        console.log(err);
                                                                    }
                                                                });
</script>