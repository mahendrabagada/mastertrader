<?php $this->load->view('include/header'); ?>
 <link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,300,600' rel='stylesheet' type='text/css'>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
<link rel="stylesheet" href="<?= base_url('assets/csslogin/style.css') ?>">  
<style type="text/css">
.tpmrgn { margin-top: 5%; margin-bottom: 9.3%; }
.button { background: #0263D4 repeat scroll 0% 0%; height: 80px; }
.button:hover { background: #1fc43a none repeat scroll 0% 0% !important; }
.tab-group .active a {
    background: #0263D4 none repeat scroll 0% 0%;
    color: #FFF;
}
a , a:hover{
    color: #0263D4
  }
.tab-group .active a:hover , .tab-group a:hover {
    background: #366193 none repeat scroll 0% 0% !important;
    color: #FFF;
}
label .req {
    margin: 2px;
    color: #0263D4;
}
label {
   color: white;
  }
</style>
<div class="form tpmrgn">
<?php $this->load->view('include/message'); ?>
      <ul class="tab-group">
        <li class="tab"><a href="#signup">Register</a></li>
        <li class="tab active"><a href="#login">Login</a></li>
      </ul>
      
      <div class="tab-content">
      <div id="login">   
          <h1>Welcome Back!</h1>
          <form action="<?= base_url('Login/do_login') ?>" method="post">
            <div class="field-wrap">
            <label>
              Username<span class="req">*</span>
            </label>
            <input type="text" id="LoginUserName" name="UserName" required autocomplete="off"/>
          </div>
          <div class="field-wrap">
            <label>
              Password<span class="req">*</span>
            </label>
            <input type="password" id="LoginPassword" name="Password" required autocomplete="off"/>
          </div>
          <p class="forgot"><a href="javascript:void(0);" data-target="#forgetpass" data-toggle="modal">Forgot Password?</a></p>
          <button class="button button-block dologin">Take me in</button>
          </form>
        </div>
        <div id="signup">   
          <h1>Register with us and enjoy trade alerts at your fingertips</h1>
          <form action="<?= base_url('Login/signup') ?>" method="post">
          <?php $planid =  isset($id)?$id:''; 
          //if($id == '' || $id == null){ ?>
          
          <div class="field-wrap" style="color: white;">
              <select name="PlanId" id="PlanId" required autocomplete="off" style="font-size:20px;">
              <option value="">Select Your Membership Plan</option>
              <?php foreach ($plan as $key => $value) { 
                foreach ($value as $key1 => $val) { ?>
              <option data-amount="<?= $val->amount  ?>" value="<?= $val->id;  ?>" <?php if($planid == $val->id) { echo 'selected="selected"'; } ?>><?= $val->plan_name." ".$val->plan_type." $".$val->amount  ?></option>
              <?php } } ?>
            </select>
          </div>
          <?php //} else { ?>
          <!-- <input type="hidden" name="PlanId" value="<?= $planid ?>"> -->
          <?php //} ?>
          <div class="top-row">
            <div class="field-wrap">
              <label>
                First Name<span class="req">*</span>
              </label>
              <input type="text" id="FirstName" name="FirstName" required autocomplete="off" />
            </div>
            <div class="field-wrap">
              <label>
                Last Name<span class="req">*</span>
              </label>
              <input type="text" id="LastName" name="LastName" required autocomplete="off"/>
            </div>
          </div>

          <div class="field-wrap">
            <label>
              Email Address<span class="req">*</span>
            </label>
            <input type="email" id="Email" name="Email" required autocomplete="off"/>
          </div>
          <div class="field-wrap">
            <label>
              Create a username<span class="req">*</span>
            </label>
            <input type="text" id="UserName" name="UserName" required autocomplete="off"/>
          </div>
          <div class="field-wrap">
            <label>
              Set A Password<span class="req">*</span>
            </label>
            <input type="password" id="Password" name="Password" required autocomplete="off"/>
          </div>
          <div class="field-wrap">
            <label>
              Confirm Password<span class="req">*</span>
            </label>
            <input type="password" id="ConfirmPassword" name="ConfirmPassword" required autocomplete="off"/>
          </div>
          <button type="submit" class="button button-block valid">Get Started</button>
          </form>
        </div>
        
      </div><!-- tab-content -->
</div> <!-- /form -->
    <div class="modal fade" id="forgetpass" tab-index="-1">
      <div class="modal-dialog" role="document" style="margin-top: 13%;">
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
              <span class="sr-only">Close</span>
            </button>
            <h4 class="modal-title">Forget Password</h4>
          </div>
          <form action="<?= base_url('Login/Forget') ?>" method="post">
            <div class="modal-body">
              <div class="form-group">
                <input type="text" id="FEmail" name="FEmail" class="form-control" placeholder="Enter Email">
              </div>
            </div>
            <div class="modal-footer">
              <div class="row">
                <div class="col-md-6">
                  <input type="submit" id="Submit" class="btn btn-primary pull-left btn-sm" style="width:50%;" name="Submit" value="Change"/>
                </div>
                <div class="col-md-6">
                    <a href="javascript:void(0);" class="btn btn-danger pull-right" data-dismiss="modal">Close</a>
                </div>
              </div>
            </div>
          </form>
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
<?php $this->load->view('include/footer'); ?>
<script src="<?= base_url('assets/jslogin/index.js') ?>"></script> 
<script>
  $(document).ready(function() {
    $('#Submit').click(function(event) {
     if(isempty('FEmail','Email') || isemail('FEmail')){
      return false;
     }
    });
  });
</script> 