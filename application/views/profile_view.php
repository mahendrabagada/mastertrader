<?php $this->load->view('include/header'); ?>
<!-- Site content -->
<div id="site-content">
    
        <div class="container">
            <div class="row">
                <div class="page-title">
                    <h2 class="title">Profile</h2>
                </div>                        
            </div><!-- /.row -->
        </div><!-- /.container -->
    
    <div id="page-body">
        <div class="information">
            <div class="container">
                <div class="row">
                  <div class="col-md-4">
                    <div class="full-width bg-transparent">
                      <div class="col-lg-12 col-md-12 col-xs-12">
                        <div class="full-width">
                          <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                            <div class="custom-form">
                              <div class="text-center bg-form">
                                <div class="img-section">
                                   <?php $new = $this->session->userdata('ProfilePicture');
                                if($this->session->userdata('NewProfilePicture') != '' ){
                                  $new = $this->session->userdata('NewProfilePicture');
                                  } ?>
                                  <img src="<?= $this->session->userdata('ProfilePicture') == $new?$this->session->userdata('ProfilePicture'):$new ?>" class="imgCircle" alt="Profile picture">
                                  <span class="fake-icon-edit" id="PicUpload" style="color: #ffffff;"><i class="fa fa-camera facamera" aria-hidden="true"></i></span>
                                </div>
                                <!-- Profile Picture -->
                                <!-- <div style="margin-top: 20%;"> -->
                                  <form id="imgform" class="form" method="post" action="<?= base_url("Profile/profilepicture"); ?>" enctype="multipart/form-data">
                                    <input type="file" id="filename" class="form-control" name="filename" />
                                  </form>
                                <!-- </div> -->
                                <!-- Profile Picture End -->
                              </div>
                              <div style="margin-top: 3%;">
                                <h4 class="btn btn-primary btn-lg"><a href="<?= base_url('Profile/upgrade') ?>" style="color: #FFF;">Upgrade/Downgrade Membership</a></h4>
                                </div>
                               <h4 class="btn btn-primary btn-lg" data-toggle="modal" data-target="#changepassword" style="padding: 10px 25px;">Change Password</h4>
                               <h4 id="cancelmember" class="btn btn-danger btn-lg" style="margin-top: 3%;">Cancel Membership</h4>
                            </div>
                          </div><!-- end col-lg-12 col-md-12 col-sm-12 col-xs-12-->
                        </div><!-- end full-width-->
                      </div><!--col-lg-12 col-md-12 col-xs-12 -->
                    </div><!-- end full-width bg-transparent-->
                  </div><!-- end col-md-4 -->
                  <div class="col-md-8 col-sm-12 col-xs-12">
                        <div class="list-group ls_grp" style="margin-top: 10px;">
                              <a href="#" class="list-group-item active us_hd">
                                USER INFORMATION
                              </a>
                              <a href="#" class="list-group-item list-group-item-action">
                                <dl class="dl-horizontal">
                                      <dt class="dt_mng">First Name</dt>
                                      <dd class="ash cap"><?= $this->session->userdata('FirstName') ?></dd>
                                </dl>
                              </a>
                              <a href="#" class="list-group-item list-group-item-action">
                                  <dl class="dl-horizontal">
                                      <dt class="dt_mng">Last Name</dt>
                                      <dd class="ash cap"><?= $this->session->userdata('LastName') ?></dd>
                                </dl>
                              </a>
                               <a href="#" class="list-group-item list-group-item-action">
                                  <dl class="dl-horizontal">
                                      <dt class="dt_mng">Date of Registration </dt>
                                      <dd><?= $this->session->userdata('CreatedOn') ?></dd>
                                </dl>
                              </a>
                               <a href="#" class="list-group-item list-group-item-action">
                                  <dl class="dl-horizontal">
                                      <dt class="dt_mng">Email Id </dt>
                                      <dd class="e_mg"><?= $this->session->userdata('Email') ?></dd>
                                </dl>
                              </a>
                              <a href="#" class="list-group-item list-group-item-action">
                                  <dl class="dl-horizontal">
                                      <dt class="dt_mng">Username</dt>
                                      <dd><?= $this->session->userdata('UserName') ?></dd>
                                </dl>
                              </a>
                        </div>
                        <div class="list-group">
                              <a href="#" class="list-group-item active us_hd">
                                MEMBERSHIP INFORMATION
                              </a>
                              <a href="#" class="list-group-item list-group-item-action">
                                <dl class="dl-horizontal">
                                      <dt class="dt_mng">Plan</dt>
                                      <dd class=""><?= $plan->plan_name ?></dd>
                                </dl>
                              </a>
                              <a href="#" class="list-group-item list-group-item-action">
                                  <dl class="dl-horizontal">
                                      <dt class="dt_mng">Frequency</dt>
                                      <dd class=""><?= $plan->plan_type ?></dd>
                                </dl>
                              </a>
                               <a href="#" class="list-group-item list-group-item-action">
                                  <dl class="dl-horizontal">
                                      <dt class="dt_mng">Amount Paid</dt>
                                      <dd class="">$<?= $plan->amount ?></dd>
                                </dl>
                              </a>
                               <a href="#" class="list-group-item list-group-item-action">
                                  <dl class="dl-horizontal">
                                      <dt class="dt_mng">Plan Validity</dt>
                                      <dd class=""><?= date("d/m/Y", strtotime($this->session->userdata('SubExpDate'))) ?></dd>
                                </dl>
                              </a>
                        </div>
                    </div><!-- end col-md-8 -->
                </div><!-- end row -->
              </div><!-- end container -->
        </div><!-- end end information-->
      </div><!-- /.page-body -->
</div><!-- /#site-content -->
    <div class="modal fade" id="changepassword">
      <div class="modal-dialog" role="document">
        <div class="modal-content topmargin">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
              <span class="sr-only">Close</span>
            </button>
            <h4 class="modal-title">Modal title</h4>
          </div>
          <div class="modal-body">
            <div class="row">
              <div class="col-lg-12">
                <form method="post" name="change" action="<?= base_url('Profile/change_password'); ?>">
                      <div class="form-group">
                      <label>Old Password</label>
                        <input class="form-control" name="Old_Password" id="Old_Password" type="password" placeholder="Old Password"/>
                      </div><br>
                      <label>New Password</label>
                      <div class="form-group">
                        <input class="form-control" name="Password" id="Password" type="password" placeholder="New Password"/>
                      </div><br>
                      <label>Retype Password</label>
                      <div class="form-group">
                        <input class="form-control" name="Re_Password" id="Re_Password" type="password" placeholder="Retype New Password"/>
                      </div><br>
                      <div class="form-group">      
                        <input type="submit" id="change_pwd" name="change_pwd" class="btn btn-success check" value="Change Password" />
                        <a href="javascript:void(0);" style="width:140px;background-color: #fff;border-radius:0px;font-weight:bold;color:#0071BD" name="clear" class="btn btn-primary pull-right btn-lg" data-dismiss="modal">Cancel</a>
                        <br>
                    </div>
                  </form>
              </div><!-- col-md-6-->
            </div><!-- row -->
          </div>
          <!-- <div class="modal-footer"> -->
            <!-- <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Save changes</button> -->
          <!-- </div> -->
        </div><!-- /.modal-content -->
      </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->
<?php $this->load->view('include/footer'); ?>
<script>
$(document).ready(function() {
    $(".check").click(function () {
    if(isempty('Old_Password','Old Password') || isempty('Password','Password') || isempty('Re_Password','Retype Password')){
         return false;
    }
    else if($("#Password").val() != $("#Re_Password").val())
    {
      alert('Password and Retype Password Not Matched');
      onfocus('Re_Password');
      return false;
    }
  });
    $('#filename').change(function(event) {
      var ext = $('#filename').val().split('.').pop().toLowerCase();
      if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
        swal({title:"<h6 style='color:#d9534f;'>Invalid Extension !</h6>",html:true});
        $(this).val('').clone();
        return false;
      } else {
        $('#imgform').submit();
      }
    });
    $(this).on('click','#cancelmember', function(){
            var data=$(this).data('id');
        swal({title: "Are you sure?",text: "Want To Delete This Record!",type: "warning",showCancelButton: true,confirmButtonColor: '#DD6B55',
                        confirmButtonText: 'Yes',cancelButtonText: "No",closeOnConfirm: false,closeOnCancel: false
       },
       function(isConfirm){
         if (isConfirm){
          window.location.replace("<?= base_url('Profile/cancelmembership') ?>");
          } else {
            swal({title:"<h6 style='color:#d9534f;'>You Membership Is Safe !</h6>",html:true});
          }
       });
      });//sweet click event
});
</script>