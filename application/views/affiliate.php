<?php $this->load->view('include/header'); ?>
<style type="text/css">
.font_weight_div p span{
	font-weight: 500;
}
.h1_style{
	color: #1565C0;
	font-weight: 500;
        margin-top: 80px;
}
.btn-style{
	margin-top: 10px;
	padding:0px 30px 0px 30px;
	font-size: 25px;
	height: 60px;
	background-color:#43A047;
}
.btn-style:hover{
	background-color:#43A047;
	color: #FFF;
}
.margin{
	margin-top:20%;
}
.how_it_work_p{
	font-size: 20px;
}
.group-button {
    margin-bottom: 20px;
}
.table .pricing-list img { height: 180px; width:180px; }
@media only screen and (max-width: 995px) {
   .margin{
		margin-top:0%;
	}
}

</style>
<div id="site-content"> 
            <div id="page-body">
                <div class="flat-row pad-top90px pad-bottom90px hd_add">
                    <div class="container">
                        <div class="row">
                           <div class="col-md-12 text-center font_weight_div">
								<h1 class="h1_style" ><p>Make money advertising our service!</p></h1>
								<h3>
									<p style="font-weight: 200;">Earn  <b><span style="color: #00a983">30% commission</span></b> on every successful referral. Residual. That's crazy!</p>
								</h3>
								<a href="<?=base_url('Affiliate/JoinUS')?>"><button type="button" class="btn btn-style group-button">Join Now</button></a>
							</div>	
                           <div class="col-md-12 adver_rd">
					      <div class="col-md-4">
						<div class="iconbox style_2">
						   <div class="box-header">
								<div class="box-icon righ_arr">
								  <i class="fa fa-desktop"></i>                
								</div>				  
							</div>
							<div class="box-content">
							<span class="font-size-14px">Advertise products on your website</span>
							   </div>
						    </div>
							</div>
                         <div class="col-md-4">
							<div class="iconbox style_2">
							   <div class="box-header">
								<div class="box-icon righ_arr">
								  <i class="fa fa-group"></i>
			                                         
								</div>
							  
							</div>
							<div class="box-content">
						<span class="font-size-14px">Visitors follow Affiliate link</span>
						   </div>
						    </div>
				</div>
                <div class="col-md-4">
					<div class="iconbox style_2">
					   <div class="box-header">
						<div class="box-icon">
						  <i class="fa fa-usd"></i>
						</div>
						</div>
						<div class="box-content">
							<span class="font-size-14px">Earn  30% on every successful referral residually! </span>
					   </div>
				    </div>
				</div>
			</div><!-- /.col-md-12 -->
            </div><!-- /.row -->
        </div><!-- /.container -->
    </div><!-- /.flat-row -->
</div><!-- /.page-body -->
</div>
<!-- <div class="container margin">
	<div class="row">
		<div class="col-md-12 text-center font_weight_div">
			<h1 class="h1_style" ><p>Make money advertising our service!</p></h1>
			<h3>
				<p style="font-weight: 200;">Earn upto <b><span style="color: #F02945">15% commission</span></b> on every product</p>
			</h3>
			<a href="<?=base_url('Affiliate/JoinUS')?>"><button type="button" class="btn btn-style">Join now</button></a>
		</div>	
	</div>
</div>
<div class="container-fluid" style="margin-top: 25px;background-color:#F6EDFE">
	<div class="row" style="margin-top:80px;">
          <div class="col-md-4 col-sm-4  wow bounceIn">
            <div class="table text-center">          
              <div class="pricing-list">                
                <img alt="Affiliate image" src="<?=base_url('')?>assets/images/affiliate/1.png" alt="">
              </div>    
              <p class="how_it_work_p">Advertise product on your website</p>
            </div>            
          </div>
          
          <div  class="col-md-4 col-sm-4">
	          <div class="table text-center">
	            <div class="pricing-list">
	          		<img alt="Affiliate image" src="<?=base_url('')?>assets/images/affiliate/2.png" alt="">
	          	</div>
	          	<p class="how_it_work_p">Visitors follow Affiliate link</p>
	          </div>
          </div>

          <div class="col-md-4 col-sm-4 wow bounceIn">
            <div class="table text-center">           
              <div class="pricing-list">                
                  <img alt="Affiliate image" src="<?=base_url('')?>assets/images/affiliate/3.png" alt="">               
              </div>
              <p class="how_it_work_p">Visitors follow Affiliate link</p>
            </div>
          </div>
    </div>
</div> -->

	
<?php $this->load->view('include/footer'); ?>