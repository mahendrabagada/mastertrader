//empty value focus border become red
function isemptyfocus(name)
{
  if($('#'+name).val().trim().length<=0)
  {
    $('#'+name).focus().css({
      'border-color':' red', '-webkit-border-radius': '4px','-moz-border-radius': '4px','border-radius': '4px','-webkit-box-shadow': '0px 0px 4px red','-moz-box-shadow': '0px 0px 4px red','box-shadow': '0px 0px 4px red'
    });
    return true;        
    }else{
      return false;
    }
}
function onfocus(name)
{    
   $('#'+name).focus().css({
    'border-color':' red',
    '-webkit-border-radius': '4px',
     '-moz-border-radius': '4px',
          'border-radius': '4px',
  '-webkit-box-shadow': '0px 0px 4px red',
     '-moz-box-shadow': '0px 0px 4px red',
          'box-shadow': '0px 0px 4px red'
  });       
}
//empty value message display in sweet alert for id
function isempty(name,message)
{
  if($('#'+name).val().trim().length<=0)
  {
    $('#'+name).focus();
    swal({title:"<h6 style='color:#d9534f;'>Please Enter "+message+"!</h6>",html:true});
    return true;        
  }else{
    return false;
  }
}
//empty value message display in sweet alert for class
function isemptyvalue(name,message)
{
  if($('.'+name).val().trim().length<=0)
  {
    $('#'+name).focus();
    swal({title:"<strong style='color:#d9534f;'>Please Enter "+message+"!</strong>",html:true});
    return true;        
  }else{
    return false;
  }
}
//select empty
function isemptyselect(name,message)
{
  if($('#'+name).val() == 'Select' || $('#'+name).val() == '')
  {
    $('#'+name).focus();
      swal({title:"<h6 style='color:#d9534f;'>Please Select "+message+"!</h6>",
        html:true
      });
     return true;        
  }else{
    return false;
  }
}
//email
function isemail(name,message="Valid Email")
{
if (!($('#'+name).val()).match(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/))
 {
    $('#'+name).focus();
    swal({title:"<h6 style='color:#d9534f;'>Please Enter "+message+"!</h6>",html:true});
     return true;        
  }else{
    return false;
  }
}
//match password to confirm password
function confirmpassword(password,confirmpassword)
{
if (($('#'+password).val())!=($('#'+confirmpassword).val()))
 {
  $('#'+confirmpassword).focus();
  swal({title:"<strong style='color:#d9534f;'>Password Does Not Match Please Re-Enter Password!</strong>",
      html:true
  });
  return true;        
  }else{
    return false;
  }
}
//Number only
function isnumber(name,message="Valid Value")
{
if (!($('#'+name).val()).match(/^[+-]?\d+(\.\d+)?$/))
 {
  $('#'+name).focus();
    swal({title:"<strong style='color:#d9534f;'>Please Enter "+message+"!</strong>",html:true});
    return true;        
  }else{
    return false;
  }
}
//Number only no - or + allowed
function isonlynumber(name,message="Valid Value")
{
if (!($('#'+name).val()).match(/^\d+(\.\d+)?$/))
 {
  $('#'+name).focus();
    swal({title:"<strong style='color:#d9534f;'>Please Enter "+message+"!</strong>",
      html:true
    });
    return true;        
  }else{
    return false;
  }
}
//Validate stdcode
function isstdno(name,message= 'Valid Std Code')
{
  if (!($('#'+name).val()).match(/^[0-9]\d{3,4}$/))
  {
    $('#'+name).focus();
    swal({title:"<strong style='color:#d9534f;'>Please Enter "+message+"!</strong>",
      html:true
    });
    return true;        
  }else{
    return false;
  }
}
//Validate Landline Number
function islandlineno(name,message= 'Valid Landline Number')
{
  if (!($('#'+name).val()).match(/^[0-9]\d{5,6}$/))
  {
    $('#'+name).focus();
    swal({title:"<strong style='color:#d9534f;'>Please Enter "+message+"!</strong>",
      html:true
    });
     return true;        
  }else{
    return false;
  }
}
//Mobile Number with country code and 10 digits
function ismobile(name,message="Valid Value")
{
if (!($('#'+name).val()).match(/^([0|\+[0-9]{1,5})?([7-9][0-9]{9})$/))
{
  $('#'+name).focus();
  swal({title:"<strong style='color:#d9534f;'>Please Enter "+message+"!</strong>",
    html:true
  });
  return true;  
  }else{
    return false;
  }
}
//Mobile Number only 10 digits 
function ismobile10(name,message="Valid Value")
{
if (!($('#'+name).val()).match(/^([7-9][0-9]{9})$/))
 {
  $('#'+name).focus();
  swal({title:"<strong style='color:#d9534f;'>Please Enter "+message+"!</strong>",
    html:true
  });
  return true;        
  }else{
    return false;
  }
}
// Validation for ip address
function isipaddress(name,message = 'Valid Ip')
{
  if (!($('#'+name).val()).match(/^(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/))
  {
    $('#'+name).focus();
    swal({title:"<strong style='color:#d9534f;'>Please Enter "+message+"!</strong>",
    html:true});
    return true;        
  }else{
    return false;
  }
}
//character only
function ischar(name,message="Valid Value")
{
if (!($('#'+name).val()).match(/^[a-zA-Z]+$/))
 {
    swal({title:"<strong style='color:#d9534f;'>Please Enter "+message+"!</strong>",
      html:true
    });
     return true;        
  }else{
    return false;
  }
}
//no space allowed in string it allowed other all characters/number/specialchar etc.
function nospace(name,message="Valid Value")
{
var regexp = /^\S*$/;
if (!($('#'+name).val()).match(regexp))
 {
    swal({title:"<strong style='color:#d9534f;'>Please Enter "+message+"!</strong>",
      html:true
    });
     return true;        
  }else{
    return false;
  }
}
// enter characters words only no numbers allowed
function isword_char(name,message="Valid Value")
{
if (!($('#'+name).val()).match(/^([a-zA-Z]+\s)*[a-zA-Z]+$/))
 {
  $('#'+name).focus();
    swal({title:"<strong style='color:#d9534f;'>Please Enter "+message+"!</strong>",html:true});
    return true;        
  }else{
    return false;
  }
}
// enter words only characters & number no allowed spacial character eg @,!,* etc.
function isnospecialchar(name,message="Valid Value")
{
if (!($('#'+name).val()).match(/^[\w\s\,]*$/i))
 {
  $('#'+name).focus();
  swal({title:"<strong style='color:#d9534f;'>Please Enter "+message+"!</strong>",html:true});
     return true;        
  }else{
    return false;
  }
}
//**************************************************************************************************************************************
//****************************** On Key Press Validation ******************************************************************************
//**************************************************************************************************************************************
// it allows to press only numeric key
function only_number_press(name)
{
  $(document).on('keypress','#'+name,function (e) {
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
      return false;
    }
  });
}
// it allows to press only numeric key
function only_alfabet_press(name)
{
  $(document).on('keypress','#'+name,function(event){
        var inputValue = event.which;
        if(!(inputValue >= 65 && inputValue <= 120) && (inputValue != 32 && inputValue != 0)) { 
            event.preventDefault(); 
        }
    });
}
// No Special Character Allowed On Keypress accept backspace
function no_special_char(name)
{
  $(document).on('keypress','#'+name,function (e) {
     if (e.which < 8 || (e.which > 10 && e.which < 48) || (e.which > 57 && e.which < 65) || (e.which > 90 && e.which < 97) || e.which > 122) {
      return false;
    }
  });
}
//****************************** File Validation ******************************************************************************
//only for image
function multiplefile(name)
{
  $(document).on('change','#'+name , function() {
      for (var i = 0; i < $(this).get(0).files.length; ++i) {
          var ext = $(this).get(0).files[i].name.split('.').pop().toLowerCase();
          if($.inArray(ext,['gif','png','jpg','jpeg'] ) == -1) {
            swal("Cancelled", "Invalid Extension! :: "+ext, "error");
            $(this).val('').clone();
              return false;
          }
        }
    });
}
function singlefile(name)
{
  var ext = $('#'+name).val().split('.').pop().toLowerCase();
  if($.inArray(ext, ['gif','png','jpg','jpeg']) == -1) {
    swal("Cancelled", "Invalid Extension !", "error");
    $(this).val('').clone();
      return false;
  }
}

//******************************* Active the ckeditor ****************************************
//For Ckeditor First Download Ckeditor From http://ckeditor.com/download
//Include Two JS File For This
//<script src="assets/plugins/ckeditor/ckeditor.js"></script>
//<script src="assets/plugins/ckeditor/adapters/jquery.js"></script>
//Call This Function inside document ready :: addckeditor(name)
function addckeditor(name)
{
  $('#'+name).ckeditor();
}
//****************************Add Datatable to project
//If You Have No CSS & JS File Visit :: https://datatables.net/download/packages
//Include  JS File :: jquery.dataTables.min.js & CSS File :: jquery.dataTables_themeroller.css
function adddatatable(name)
{
  $('#'+name).dataTable( {
      "bJQueryUI": true,
      "sPaginationType": "full_numbers"
  });
}
//*******************  Add Slider *****************
//Fancy Box
//For That Include Js & Css File :: CSS :: fancy/source/jquery.fancybox.css?v=2.1.5 JS :: fancy/source/jquery.fancybox.js?v=2.1.5
//If You Have No File Visit :: http://fancyapps.com/fancybox/
function addfancybox(name)
{
  $('.'+name).fancybox();
}
//*******************  Add BxSlider *****************
//BxSlider 
//For That Include Js & Css File :: CSS :: jquery.bxslider.css JS :: jquery.bxslider.min.js
//If You Have No File Visit :: https://github.com/stevenwanderski/bxslider-4
function bxslider_with_minmax_slide(name,minslide,maxslide,cls = 'class')
{
   if(cls == 'id')
   {
      $('#'+name).bxSlider({
              minSlides: minslide,
              maxSlides: maxslide,
              slideWidth: 213,
              slideHeight: 213,
              slideMargin: 10
        });
    }
    else
    {
       $('.'+name).bxSlider({
              minSlides: minslide,
              maxSlides: maxslide,
              slideWidth: 213,
              slideHeight: 213,
              slideMargin: 10
        });
    }
}
//********************** Date Time Picker bootstrap *************
//If no file download from https://github.com/Eonasdan/bootstrap-datetimepicker , also download moment.min.js from http://momentjs.com/downloads/moment.min.js
//include assets/plugins/bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css
//assets/plugins//bootstrap-datetimepicker/build/js/moment.min.js
//assets/plugins/bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js
function bootstrap_datetime_picker(name,format)
{
  //"DD-MM-YYYY HH:mm:ss"
  $('#'+name).datetimepicker({
                format: format,
                showTodayButton: true,
                showClear: true
            });
}
//*************** Date Function ***************************
//Calling This Function :: year_difference('11', '21', '2001');
function year_difference(Month, Day, Year)
{
    todayDate = new Date();
    todayYear = todayDate.getFullYear();
    todayMonth = todayDate.getMonth();
    todayDay = todayDate.getDate();
    age = todayYear - Year;
    if (todayMonth < Month - 1){
      age--;
    }
    if (Month - 1 == todayMonth && todayDay < Day){
      age--;
    }
    return age;
}
//Calling This Function :: day_difference('12/02/2016','11/20/2016') //mm/dd/yyyy
function day_difference(d1,d2)
{
  var date1 = new Date(d1);
  var date2 = new Date(d2);
  var timeDiff = Math.abs(date2.getTime() - date1.getTime());
  var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24)); 
  return diffDays;
}
//Calling This Function :: month_diffrence(new Date(2008, 10, 4), new Date(2010, 2, 12) ) //Month is 10 + 1 = 11 (Nov)
function month_diffrence(d1, d2) {
    var months;
    months = (d2.getFullYear() - d1.getFullYear()) * 12;
    months -= d1.getMonth() + 1;
    months += d2.getMonth();
    return months <= 0 ? 0 : months;
}
//Calling This Function :: hour_difference(new Date(2016, 10, 11), new Date(2016, 10, 12))
function hour_difference(date1,date2)
{
  var hours = Math.abs(date1 - date2) / 36e5;
  return hours <= 0 ? 0 : hours; 
}
//Calling This Function :: day_hour_minute_second_remain_to_feature_date(new Date(),new Date(2012,11,25))
function day_hour_minute_second_remain_to_feature_date(date_now,date_future,format)
{
  var delta = Math.abs(date_future - date_now) / 1000;
  var days = Math.floor(delta / 86400);
  delta -= days * 86400;
  var hours = Math.floor(delta / 3600) % 24;
  delta -= hours * 3600;
  var minutes = Math.floor(delta / 60) % 60;
  delta -= minutes * 60;
  var seconds = delta % 60;
  return 'Remain Days :: '+days+'Hours :: '+hours+'Minutes :: '+minutes+'Seconds  :: '+seconds;
}
//************* Add Multiple Select
//For This You Want To Download Files From :: https://github.com/davidstutz/bootstrap-multiselect
//One Important thing is you want to define multiple in select Like :: <select id="id" name="name" multiple>
//For Options Visit :: http://davidstutz.github.io/bootstrap-multiselect/
//include JS File :: bootstrap-select.min.js And CSS File :: bootstrap-multiselect.css in healer
function addmultipleselect(name)
{
  $('#'+name).selectpicker();
}
//Custom Alert For Display Message
function ErrorAlert(message) {
    if(message != ""){
        $("#ErrorAlertMessage").text(message);
        $("#ErrorAlertModal").modal("show");
    }
}
function SuccessAlert(message) {
  if(message != ""){
      $("#SuccessAlertMessage").text(message);
      $("#SuccessAlertModal").modal("show");
  }
}
function ConfirmAlert(message,button) {
    if(message != "" && button != ""){
        $("#CustomConfirmAlertMessage").text(message);
        $("#CustomConfirmButton").text(button);
        $("#CustomConfirmModal").modal("show");
    }
}
//**************************************************************************************************************************
//****************************** Two date Difference , date based on first  ************************************************
//**************************************************************************************************************************
function datedifference(date1 = '',date2 = '')
{
  $( function() {
    var dateToday = new Date();
    $( "#"+date1 ).datepicker({dateFormat: "yy-mm-dd", defaultDate: "+1w",
        changeMonth: true, minDate: dateToday,
    });
    $(document).on('change','#'+date1,function(){
      var selectdate = new Date($('#'+date1).val());
        $( "#"+date2 ).datepicker({dateFormat: "yy-mm-dd", defaultDate: "+1w",
        changeMonth: true, minDate: selectdate,
      });
    });
});//document ready
}
function bootstrap_start_end_date(argument) {
  var dateToday = new Date();
  $('#StartDate').datepicker({
        format: 'dd-mm-yyyy',
        startDate: dateToday,
         autoclose: true,
        todayHighlight:true
  }).on('changeDate', function (selected) {
        var minDate = new Date(selected.date.valueOf());
        $('#EndDate').datepicker('setStartDate', minDate);
  });
  $(this).on('change','#StartDate',function(){
    var selectdate = new Date($(this).val());
      $( "#EndDate").datepicker({format: "dd-mm-yyyy",
        startDate: selectdate,
        }).on('changeDate', function (selected) {
          var minDate = new Date(selected.date.valueOf());
          $('#StartDate').datepicker('setEndDate', minDate);
        });
      });
}
//****************************** AJAX Request ******************************************************************************
//var post_data = {"CategoryID":CategoryID};
//Call This Function ajax_request("<?php echo base_url(); ?>quotation/ajax-get-inventory",data, response_request,element);
function ajax_request(URL, data, response_function, element){
    $.ajax({
        type: "POST",
        datatype:"json",
        url: URL,
        data: data,
        cache: false,
        success: function(result){
            response_function(result, element);
        },
        error: function() {
            alert('Data Request Failed.');
            response_function(undefined, element);
        }
    });
}
//if success Apend the response data to the page
function response_request(result, element){
    empty_result(element);
    if(result != undefined && result != NaN && result != ""){
        result = JSON.parse(result);
        $.each(result, function(i,item) {
          element.find('.inventory').append('<option value="'+ item.InventoryID +'"  data-price="'+ item.Price +'"  data-min-quantity="'+ item.MinQuantity +'">'+ item.InventoryName +'</option>');        });
    }
}
// If Result is empty make the value empty
function empty_result(element){
    element.find(".id").empty();
    element.find("#id").val("");
}
//var data = {"id":id};
//Call This Function ajax_request_sample("<?php echo base_url(); ?>controller/method",data,idtoappend);
function ajax_request_sample(url,data,appendval)
{
  $.ajax({
      type: 'POST',
      url: url,
      data: data,
      cache: false,
      success: function(response) 
      {
        $('#'+appendval).val(response);
      }
    });
}

//**************************************************************************************************************************************
//****************************** Other Usable Functions ********************************************************************************
//**************************************************************************************************************************************

// 1.remove spaces from strings
function removespaces(value)
{
  var str = value.replace(/\s/g,''); 
  return str;
}
// auto remove bs-alert wich affected on alert calass
function auto_remove_alert()
{
  $(document).ready(function () {
    window.setTimeout(function() {
        $(".alert").fadeTo(1000, 0).slideUp(1000, function(){
            $(this).remove(); 
        });
    }, 6000);
  });
}
// change border red color when onchange input
function remove_border_onchange()
{
  jQuery(document).ready(function($) {
    $("input").change(function(){
      $(this).css({'border-color':'','-webkit-border-radius': '','-moz-border-radius': '','border-radius': '','-webkit-box-shadow': '','-moz-box-shadow': '','box-shadow': ''});
    });
  });
}
/* ajex Loader Function */
/* 1st arguments:-
        var loader_image='<?php echo base_url('/assets/img/ajax-loader.gif'); ?>';
    2nd arguments:-
       if you want to put images then----- "var loader_text='<img src="<?php echo base_url('/assets/img/loding-text.gif'); ?>">';
       if you want to put text then------- var loader_text="Loading...";
*/
function load_ajex_loader(loader_image,loader_text)
{
function ajaxindicatorstart(text)
  {
    if(jQuery('body').find('#resultLoading').attr('id') != 'resultLoading'){
    jQuery('body').append('<div id="resultLoading" style="display:none"><div><img src="'+loader_image+'"><div>'+text+'</div></div><div class="bg"></div></div>');
    }
    jQuery('#resultLoading').css({
      'width':'100%',
      'height':'100%',
      'position':'fixed',
      'z-index':'10000000',
      'top':'0',
      'left':'0',
      'right':'0',
      'bottom':'0',
      'margin':'auto'
    });
    jQuery('#resultLoading .bg').css({
      'background':'#000000',
      'opacity':'0.7',
      'width':'100%',
      'height':'100%',
      'position':'absolute',
      'top':'0'
    });
    jQuery('#resultLoading>div:first').css({
      'width': '250px',
      'height':'75px',
      'text-align': 'center',
      'position': 'fixed',
      'top':'0',
      'left':'0',
      'right':'0',
      'bottom':'0',
      'margin':'auto',
      'font-size':'16px',
      'z-index':'10',
      'color':'#ffffff'
    });
      jQuery('#resultLoading .bg').height('100%');
        jQuery('#resultLoading').fadeIn(300);
      jQuery('body').css('cursor', 'wait');
  }       
function ajaxindicatorstop()
{
  jQuery('#resultLoading .bg').height('100%');
    jQuery('#resultLoading').fadeOut(300);
  jQuery('body').css('cursor', 'default');
}
jQuery(document).ajaxStart(function () {
      //show ajax indicator
    ajaxindicatorstart(loader_text);
  }).ajaxStop(function () {
    //hide ajax indicator
    ajaxindicatorstop();
  });
  jQuery.ajax({
   global: false,
   // ajax stuff
});
}
/* end ajex loader */
