$('.form').find('input, textarea').on('keyup blur focus', function (e) {
  
  var $this = $(this),
      label = $this.prev('label');

	  if (e.type === 'keyup') {
			if ($this.val() === '') {
          label.removeClass('active highlight');
        } else {
          label.addClass('active highlight');
        }
    } else if (e.type === 'blur') {
    	if( $this.val() === '' ) {
    		label.removeClass('active highlight'); 
			} else {
		    label.removeClass('highlight');   
			}   
    } else if (e.type === 'focus') {
      
      if( $this.val() === '' ) {
    		label.removeClass('highlight'); 
			} 
      else if( $this.val() !== '' ) {
		    label.addClass('highlight');
			}
    }

});
$('.form').on('click','.valid', function (e){
  if(isemptyselect('PlanId','Plan First') || isempty('FirstName','First Name') || isempty('LastName','Last Name') || isempty('Email','Email') || isemail('Email') || isempty('UserName','User Name')  || isempty('Password','Password') || isempty('ConfirmPassword','ConfirmPassword')){ return false; }
  else if( $('#Password').val() != $('#ConfirmPassword').val() ) {
        swal({title:"<h6 style='color:#d9534f;'>Password And Confirm Password must Be Same</h6>",html:true});
        return false;
      }
});
$('.form').on('click','.dologin', function (e){
  if(isempty('LoginUserName','User Name') || isempty('LoginPassword','Password')){ return false; }
})
$('.tab a').on('click', function (e) {
  
  e.preventDefault();
  
  $(this).parent().addClass('active');
  $(this).parent().siblings().removeClass('active');
  
  target = $(this).attr('href');

  $('.tab-content > div').not(target).hide();
  
  $(target).fadeIn(600);
  
});